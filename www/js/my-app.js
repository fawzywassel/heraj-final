// Initialize your app
var myApp = new Framework7({
	animateNavBackIcon: true,
	swipeBackPage: false,
	swipePanel: false,
	cache: false,
	/* disable caching */
	cacheDuration: 0,
	/* set caching expire time to 0 */
	template7Pages: false,
	onPageInit: checkConnection()
});

myApp.params.cache = false;

function checkConnection() {
	if (!window.navigator.onLine) {
		myApp.alert("check your connection and go back", "Connection Error", gotoSetting())
	}
}

function gotoSetting() {
	cordova.plugins.diagnostic.switchToSettings(function () {
		console.log("Successfully switched to Settings app");
	}, function (error) {
		console.error("The following error occurred: " + error);
	});
}

myApp.params.cache = false; // could also be false to disable any caching

$("#select_category_upload_ad").val("6");

myApp.onPageBeforeAnimation('*', function (page) {
	//$$('.view-main .page-on-left, .view-main .navbar-on-left').remove();
	//alert("back");
	//page.view.router.refreshPage();
});


//Initialize the Firebase SDK
var config = {
	apiKey: "AIzaSyB0dfH7mWv_wl7e7N4j0cbGJt2HZi2JyM8",
	authDomain: "heraj-4b33c.firebaseapp.com",
	databaseURL: "http://heraj-4b33c.firebaseio.com/",
	storageBucket: '<your-storage-bucket>'
};

firebase.initializeApp(config);



// Export selectors engine
var $$ = Dom7;

// Add main View
var mainView = myApp.addView('.view-main', {
	// Enable dynamic Navbar
	dynamicNavbar: true,
	// Enable Dom Cache so we can use all inline pages
	domCache: true
});



// Open more popover
$$('.open-more').on('click', function () {
	var clickedLink = this;
	myApp.popover('.popover-more', clickedLink);
});


myApp.removeFromCache('advertisement');

// Option 1. Using page callback for page (for "about" page in this case) (recommended way):

myApp.onPageBack('advertisementDetails', function (page) {
	//alert("details");
	//page.view.router.refreshPage();
	//myApp.removeFromCache('advertisementDetails');
	//mainView.router.refreshPage(); 
	//$$('.page-on-right').remove();
})
myApp.onPageInit('halal', function (page) {
	// Do something here for "about" page
	//alert("halal");
	if (localStorage.getItem("name") == "undefined" || localStorage.getItem("name") == null) {


	} else {

	}

})


myApp.onPageBeforeAnimation('advertisementDetails', function (page) {

	//if (page.from === 'left') {
	page.view.router.refreshPage();
	//}
});


function clearadvDetailsPage() {

}





/**
 *  Navigation Functions Section
 * ====================================================================================== */
function changeToLogin() {
	mainView.router.load({
		pageName: 'login'
	});
}

function goToSignUp() {
	//alert("go to login myapp.js");
	mainView.router.load({
		pageName: 'signup'
	});
}

function goToChat() {
	//alert("go to login myapp.js");
	mainView.router.load({
		pageName: 'chat'
	});
}



function goToEditComment() {
	$("#comment_title").text("تعديل رد");
	mainView.router.load({
		pageName: 'editComment'
	});
}


function goToAddComment() {
	//alert("اضافة رد");
	$("#comment_title").text("إضافة رد");
	mainView.router.load({
		pageName: 'editComment'
	});
}


function changeToVerfication() {
	//alert("go to login myapp.js");
	mainView.router.load({
		pageName: 'verifyCode'
	});
}

function changeToCategory() {

	// alert("here")
	mainView.router.back({
		force: true
	})
	mainView.router.load({
		pageName: 'category'
	});
}

function goToBack() {
	mainView.router.back();
}



function goToAdvertisementDetails() {
	mainView.router.load({
		pageName: 'advertisementDetails'
	});
}

function goToEditAdvertisement() {
	mainView.router.load({
		pageName: 'addAdvertisement'
	});
}


function goToAdvertisement() {
	mainView.router.load({
		pageName: 'advertisement'
	});

}


function goToReportPage() {
	mainView.router.load({
		pageName: 'reportComment'
	});
}

function goToProfileEditPage() {
	//alert("go to edit");
	mainView.router.load({
		pageName: 'editProfile'
	});
}

function goToFavouritePage() {
	mainView.router.load({
		pageName: 'favouritesAdvertisement'
	});
}

function goTomyAdvertisementPage() {
	mainView.router.load({
		pageName: 'myAdvertisement'
	});

}

function goToVehiclesSearch() {
	mainView.router.load({
		pageName: 'vehicles'
	});
}

//== Bottom Bar Navigation
function goToProfilePage() {
	mainView.router.load({
		pageName: 'profile'
	});
}

function goToSearchPage() {
	mainView.router.load({
		pageName: 'search'
	});
}

function goToHomePage() {
	mainView.router.load({
		pageName: 'category'
	});
}

function goToPublishAdPage() {
	mainView.router.load({
		pageName: 'addAdvertisement'
	});
}

function goToNotificationPage() {
	mainView.router.load({
		pageName: 'notification'
	});
}


function goToMessagesPage() {
	mainView.router.load({
		pageName: 'messages'
	});
}


function refreshPreviousPage() {
	//alert("refreash");

	//mainView.router.refreshPreviousPage();
	//mainView.router.refreshPage();
	//mainView.router.reloadPreviousPage('#advertisementDetails');
}



/* End Of Navigation Functions 
---------------------------------------------------------------------------------------------------- */









var checked = false;

//----------------------------  Splash Timer
var splahTimer = setInterval(spTimerFunc, 2000);

function spTimerFunc() {

	if (localStorage.getItem("name") == "undefined" || localStorage.getItem("name") == null || localStorage.getItem("name") == "") {
		//alert("go to login");
		$("div[data-page='category']").removeClass("cached");
		changeToLogin();
		//window.location.hash = "#login";
		//location.hash = "#login";
		/*window.location.hash = '#login';
		location.reload();*/
		clearInterval(splahTimer);


	} else {
		if (localStorage.getItem("token") == "undefined" || localStorage.getItem("token") == null || localStorage.getItem("name") == "") {
			changeToLogin();
		} else {
			changeToCategory();
		}

		clearInterval(splahTimer);
	}

}





//---------------------------------------- Delete Comment
function deletComment() {
	showProgressBoth();
	$.ajax({
		url: BASE_URL_ROOT + "/comments/" + localStorage.getItem("commentId") + "?token=" + getToken(),
		data: "",
		processData: false,
		contentType: false,
		type: 'DELETE',
		success: function (data) {

			hideProgressBoth();

			makeToast("تم الحذف");

		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			//alert("Status: " + textStatus);
			hideProgressBoth();
			makeToast(JSON.parse(XMLHttpRequest.responseText).message);
		}
	});



}


function profileClicked() {
	makeToast("clicked");
}



//----------------------------------------   FirebasePlugin Handling
document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {

	document.addEventListener("backbutton", backButtonHandler, false);




	var notificationOpenedCallback = function (jsonData) {
		console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
	};

	window.plugins.OneSignal
		.startInit("35f5e23f-de79-4df7-adfc-19168426c5d2")
		.handleNotificationOpened(notificationOpenedCallback)
		.endInit();





	// //------------------------------ FirebasePlugin Plugin
	// //---------------------------------------------------------------
	// window.FirebasePlugin.getToken(function (token) {
	// 		// save this server-side and use it to push notifications to this device
	// 		//alert(token);
	// 	},
	// 	function (error) {
	// 		console.error(error);
	// 	}
	// );


	// window.FirebasePlugin.onTokenRefresh(function (token) {
	// 	// save this server-side and use it to push notifications to this device
	// 	//alert("refToken " + token);
	// }, function (error) {
	// 	console.error(error);
	// });


	// window.FirebasePlugin.onNotificationOpen(function (notification) {
	// 	//alert(notification);
	// }, function (error) {
	// 	console.error(error);
	// });






	// //------------------------------ FCMPlugin Plugin
	// //---------------------------------------------------------------
	// FCMPlugin.onTokenRefresh(function (token) {
	// 	//alert( token );
	// });

	// //FCMPlugin.getToken( successCallback(token), errorCallback(err) );
	// //Keep in mind the function will return null if the token has not been established yet.
	// FCMPlugin.getToken(function (token) {
	// 	//alert(token);
	// });

	// FCMPlugin.subscribeToTopic('topicExample');

	// //FCMPlugin.unsubscribeFromTopic( topic, successCallback(msg), errorCallback(err) );
	// FCMPlugin.unsubscribeFromTopic('topicExample');

	// //FCMPlugin.onNotification( onNotificationCallback(data), successCallback(msg), errorCallback(err) )
	// //Here you define your application behaviour based on the notification data.
	// FCMPlugin.onNotification(function (data) {
	// 	//alert( "data is : " + data.notification.body );
	// 	makeToast(data.notification.body);
	// 	console.log("data is : " + JSON.stringify(data));
	// 	if (data.wasTapped) {
	// 		//Notification was received on device tray and tapped by the user.
	// 		//alert( JSON.stringify(data) );
	// 	} else {
	// 		//Notification was received in foreground. Maybe the user needs to be notified.
	// 		alert(JSON.stringify(data));
	// 	}
	// });


}


function backButtonHandler(e) {
	//Here implement the back button handler
	//makeToast("back");
	goToBack();
}




//------------------------------------------------- Progress Dialogs
function showProgressAndroid() {
	cordova.plugin.pDialog.init({
		theme: 'HOLO_DARK',
		progressStyle: 'SPINNER',
		cancelable: true,
		title: 'Please Wait...',
		message: 'Contacting server ...'
	});

}

function hideProgressAndroid() {
	//window.plugins.spinnerDialog.show(null, "...جارى الإرسال");
	cordova.plugin.pDialog.dismiss();
}



function showProgressBoth() {
	//window.plugins.spinnerDialog.show(null, "...جارى الإرسال");
	//window.plugins.spinnerDialog.show("حراج","جارى التحميل ...", true);
	//$cordovaProgress.showSimple(true); // requires .hide()
	window.plugins.spinnerDialog.show(null, null, true);
}

function hideProgressBoth() {
	window.plugins.spinnerDialog.hide();
}





//------------------------------------------------ Toast Plugin Hanlding
function makeToast(msg) {
	window.plugins.toast.showWithOptions({
			message: msg,
			duration: 1500, // ms
			position: "bottom",
			addPixelsY: -40, // (optional) added a negative value to move it up a bit (default 0)
			data: {
				'foo': 'bar'
			} // (optional) pass in a JSON object here (it will be sent back in the success callback below)
			,
			styling: {
				backgroundColor: '#fd8e22', // orange
				borderRadius: 20, // a bit less than default, 0 means a square Toast
				alpha: 200 // 0-255, 0 being fully transparent

			}

		}, function () {

		}, function (e) {
			// alert(e);
			// alert('error');
		}

	);
}




/*  			Start Of The Native Js/Jquery Code
-----------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------*/
var BASE_URL = "haraj.voxelvention.com/api";

var fd = new FormData();
var fd_user = new FormData();

var fd_upload_Ad = new FormData();


var images = [];


$("#file_of_img1").click(function () {});

function callImage1() {
	$('#file_of_img1').trigger('click');
}


function readImgUser(input) {
	fd_user.append('image', input.files[0]);

	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#img_user')
				.attr('src', e.target.result)
				.width(150)
				.height(200);

			//localStorage.setItem("imgData", getBase64Image(e.target.result));
		};


		reader.readAsDataURL(input.files[0]);
	}

}

function getImageUser() {
	return fd_user.get('image');
}




//--------------------------------------------------- Read All Images As Files 
function readImg(input) {
	images.push(input.files[0]);
	fd.append('image', input.files[0]);

	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#img')
				.attr('src', e.target.result)
				.width(150)
				.height(200);

			//localStorage.setItem("imgData", getBase64Image(e.target.result));
		};


		reader.readAsDataURL(input.files[0]);
	}


}

function readImg1(input) {

	images.push(input.files[0]);
	fd.append('image1', input.files[0]);

	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#img1')
				.attr('src', e.target.result)
				.width(150)
				.height(200);

			//localStorage.setItem("imgData", getBase64Image(e.target.result));
		};


		reader.readAsDataURL(input.files[0]);
	}


}

function readImg2(input) {
	images.push(input.files[0]);
	fd.append('image2', input.files[0]);

	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#img2')
				.attr('src', e.target.result)
				.width(150)
				.height(200);

			//localStorage.setItem("imgData", getBase64Image(e.target.result));
		};


		reader.readAsDataURL(input.files[0]);
	}


}

function readImg3(input) {
	images.push(input.files[0]);
	fd.append('image3', input.files[0]);

	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#img3')
				.attr('src', e.target.result)
				.width(150)
				.height(200);

			//localStorage.setItem("imgData", getBase64Image(e.target.result));
		};


		reader.readAsDataURL(input.files[0]);
	}


}

function readImg4(input) {
	images.push(input.files[0]);
	fd.append('image4', input.files[0]);

	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#img4')
				.attr('src', e.target.result)
				.width(150)
				.height(200);

			//localStorage.setItem("imgData", getBase64Image(e.target.result));
		};


		reader.readAsDataURL(input.files[0]);
	}


}

function readImg5(input) {
	images.push(input.files[0]);
	fd.append('image5', input.files[0]);

	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#img5')
				.attr('src', e.target.result)
				.width(150)
				.height(200);

			//localStorage.setItem("imgData", getBase64Image(e.target.result));
		};


		reader.readAsDataURL(input.files[0]);
	}


}

function readImg6(input) {
	images.push(input.files[0]);
	fd.append('image6', input.files[0]);

	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#img6')
				.attr('src', e.target.result)
				.width(150)
				.height(200);

			//localStorage.setItem("imgData", getBase64Image(e.target.result));
		};


		reader.readAsDataURL(input.files[0]);
	}


}


function readImg7(input) {
	images.push(input.files[0]);
	fd.append('image7', input.files[0]);

	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#img7')
				.attr('src', e.target.result)
				.width(150)
				.height(200);

			//localStorage.setItem("imgData", getBase64Image(e.target.result));
		};


		reader.readAsDataURL(input.files[0]);
	}


}

function readImg8(input) {
	images.push(input.files[0]);
	fd.append('image8', input.files[0]);

	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#img8')
				.attr('src', e.target.result)
				.width(150)
				.height(200);

			//localStorage.setItem("imgData", getBase64Image(e.target.result));
		};


		reader.readAsDataURL(input.files[0]);
	}


}

function readImg9(input) {
	images.push(input.files[0]);
	fd.append('image9', input.files[0]);

	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#img')
				.attr('src', e.target.result)
				.width(150)
				.height(200);

			//localStorage.setItem("imgData", getBase64Image(e.target.result));
		};


		reader.readAsDataURL(input.files[0]);
	}


}

function readImg10(input) {
	images.push(input.files[0]);
	fd.append('image10', input.files[0]);

	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#img10')
				.attr('src', e.target.result)
				.width(150)
				.height(200);

			//localStorage.setItem("imgData", getBase64Image(e.target.result));
		};


		reader.readAsDataURL(input.files[0]);
	}


}

//--------------------------------------------------- End Of Read All Images As Files 





function appendImagesUrls(key, value) {
	fd.append(key, value);
}


//--------------------------------------- Convert img tag to Base64  (Not Used Now)
function getBase64Image(img) {
	var canvas = document.createElement("canvas");
	canvas.width = img.width;
	canvas.height = img.height;
	var ctx = canvas.getContext("2d");
	ctx.drawImage(img, 0, 0);
	var dataURL = canvas.toDataURL("image/png");
	// alert(dataURL.replace(/^data:image\/(png|jpg);base64,/, ""));
	console.log("imgbase64 " + dataURL.replace(/^data:image\/(png|jpg);base64,/, ""));
	return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");


}



//--------------------------------------- Another Convert img tag to Base64  (Not Used Now)
imageBase64 = function (url, callback, outputFormat) {
	var canvas = document.createElement('CANVAS'),
		ctx = canvas.getContext('2d'),
		img = new Image;
	img.crossOrigin = 'Anonymous';
	img.src = url;
	img.onload = function () {
		var dataURL;
		canvas.height = img.height;
		canvas.width = img.width;
		ctx.drawImage(img, 0, 0);
		dataURL = canvas.toDataURL("image/png"); // the base64 data of the image
		dataURL = dataURL.substring("data:image/png;base64,".length);
		callback.call(this, dataURL);
		canvas = null;
	};
}



// Fill the form of the add upload/edit with the main image
function attachImage(imgData) {
	//alert("image main");
	fd.set('image', imgData.replace(/^data:image\/[a-z]+;base64,/, ""));
}


// Counter for the 10 images
var counter = 0;


// Clear the counter after come back form the image picker
function clearCounter() {
	counter = 0;
}

// Fill the form of the add upload/edit with the 10 images
function attachImages(imgData) {
	//alert("image counter " + counter);
	if (counter == 0) {
		fd.set('image1', imgData.replace(/^data:image\/[a-z]+;base64,/, ""));
	} else if (counter == 1) {
		fd.set('image2', imgData.replace(/^data:image\/[a-z]+;base64,/, ""));
	} else if (counter == 2) {
		fd.set('image3', imgData.replace(/^data:image\/[a-z]+;base64,/, ""));
	} else if (counter == 3) {
		fd.set('image4', imgData.replace(/^data:image\/[a-z]+;base64,/, ""));
	} else if (counter == 4) {
		fd.set('image5', imgData.replace(/^data:image\/[a-z]+;base64,/, ""));
	} else if (counter == 5) {
		fd.set('image6', imgData.replace(/^data:image\/[a-z]+;base64,/, ""));
	} else if (counter == 6) {
		fd.set('image7', imgData.replace(/^data:image\/[a-z]+;base64,/, ""));
	} else if (counter == 7) {
		fd.set('image8', imgData.replace(/^data:image\/[a-z]+;base64,/, ""));
	} else if (counter == 8) {
		fd.set('image9', imgData.replace(/^data:image\/[a-z]+;base64,/, ""));
	} else if (counter == 9) {
		fd.set('image10', imgData.replace(/^data:image\/[a-z]+;base64,/, ""));
	}


	counter = counter + 1;

}




/** Upload Ad Function that send the form of the ad publish page
======================================================================================== */
function alertFormData(upload_type) {

	//makeToast("number of photos " + localStorage.getItem("images_length"));

	console.log(fd.get('image'));
	console.log(fd.get('image1'));
	console.log(fd.get('image2'));
	console.log(fd.get('image3'));
	console.log(fd.get('image4'));
	console.log(fd.get('image5'));

	fd.append("images", images);
	fd.append("title", $('#ad_title').val());
	fd.append("description", $('#ad_description').val());


	if ($('#ad_title').val() == "" || $('#ad_title').val() == null || $('#ad_description').val() == "" || $('#ad_description').val() == null) {
		makeToast("برجاء إكمال البيانات");
		return;
	}


	if (typeof $("#select_subcategory_upload_ad").children(":selected").attr("id") != 'undefined') {

		fd.append("terms_ids", $("#select_category_upload_ad").children(":selected").attr("id") + "," + $("#select_subcategory_upload_ad").children(":selected").attr("id"));

	} else {
		//alert("sub is undefined");
		//makeToast("sub is undefined");
		/*-----------------------------------------------------------------------/
		The vehicle case and 6 is the vehicle id
		Note that we need to check that only when no subcategories selected
		So in the next else there is SUBCATEROTY which ofcourse it is not vehicle
		So ne need to check the terms of the vehicles brand,model,type
		---------------------------------------------------------------------------------*/
		if ($("#select_category_upload_ad").children(":selected").attr("id") == "6") {
			var terms_ids_vehciles = getTermsForVehicles();
			//alert("var: " + terms_ids_vehciles);
			fd.append("terms_ids", $("#select_category_upload_ad").children(":selected").attr("id") + "," + terms_ids_vehciles);
		} else {
			fd.append("terms_ids", $("#select_category_upload_ad").children(":selected").attr("id"));
		}
	}


	if (fd.get('image') == null) {
		makeToast("برجاء اختيار صورة رئيسية");
	} else {
		if ($('#ad_check').is(':checked')) {


			if (upload_type == "edit") {

				$('#picOpt').hide();


				console.log("OKOK" + "edit ad Id: " + 'http://haraj.voxelvention.com/api/posts/' + localStorage.getItem("adId") + '?token=' + getToken());
				console.log("OKOK" + fd.get("image"));
				console.log("OKOK" + fd.get("image1"));

				showProgressBoth();

				$.ajax({
					url: 'http://haraj.voxelvention.com/api/posts/' + localStorage.getItem("adId") + '?token=' + getToken(),
					data: fd,
					processData: false,
					contentType: false,
					type: 'POST',
					success: function (data) {

						hideProgressBoth();

						makeToast("تم التعديل بنجاح");
						//alert(JSON.stringify(data));

						$('#ad_title').val('');
						$('#ad_description').val('');
						$('#select_category_upload_ad').prop('selectedIndex', 0);
						$('#select_subcategory_upload_ad').prop('selectedIndex', 0);
						$('#marka_ad').prop('selectedIndex', 0);
						$('#model_ad').prop('selectedIndex', 0);
						$('#type_ad').prop('selectedIndex', 0);

						$('#img').attr('src', '');
						$('#img1').attr('src', '');
						$('#img2').attr('src', '');
						$('#img3').attr('src', '');
						$('#img4').attr('src', '');
						$('#img5').attr('src', '');
						$('#img6').attr('src', '');
						$('#img7').attr('src', '');
						$('#img8').attr('src', '');
						$('#img9').attr('src', '');
						$('#img10').attr('src', '');

						goTomyAdvertisementPage();


					},
					error: function (XMLHttpRequest, textStatus, errorThrown) {
						//alert(JSON.stringify(XMLHttpRequest));
						console.log(JSON.stringify(XMLHttpRequest));
						console.log(JSON.parse(XMLHttpRequest.responseText));
						hideProgressBoth();
						makeToast(JSON.parse(XMLHttpRequest.responseText).message);



					}
				});

			} else {
				//alert("create");

				showProgressBoth();

				$.ajax({
					url: 'http://haraj.voxelvention.com/api/posts?token=' + getToken(),
					data: fd,
					processData: false,
					contentType: false,
					type: 'POST',
					success: function (data) {
						hideProgressBoth();
						makeToast("تم الرفع بنجاح");

						$('#ad_title').val('');
						$('#ad_description').val('');
						$('#select_category_upload_ad').prop('selectedIndex', 0);
						$('#select_subcategory_upload_ad').prop('selectedIndex', 0);
						$('#marka_ad').prop('selectedIndex', 0);
						$('#model_ad').prop('selectedIndex', 0);
						$('#type_ad').prop('selectedIndex', 0);

						$('#img').attr('src', '');
						$('#img1').attr('src', '');
						$('#img2').attr('src', '');
						$('#img3').attr('src', '');
						$('#img4').attr('src', '');
						$('#img5').attr('src', '');
						$('#img6').attr('src', '');
						$('#img7').attr('src', '');
						$('#img8').attr('src', '');
						$('#img9').attr('src', '');
						$('#img10').attr('src', '');

						goToBack();
					},
					error: function (XMLHttpRequest, textStatus, errorThrown) {
						//alert("Status: " + textStatus);
						hideProgressBoth();
						makeToast(JSON.parse(XMLHttpRequest.responseText).message);


					}
				});

			}

		} else {
			makeToast("برجاء الموافقة على المعاهدة");
		}

	}

}




//== Function that return the catched Token of the user
function getToken() {
	var token = ""


	if (localStorage.getItem("token") == "undefined" || localStorage.getItem("token") == null) {
		// For that before login use that default token (for test now)
		token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzYW1wbGVfYXBwIiwiYXVkIjoic2FtcGxlIF9hcHAgdXNlciIsImlhdCI6MTUwMjM1NDA2MywibmJmIjoxNTAyMzU0MDYzLCJleHAiOjE1MDI0NDA0NjMsInVzZXJJZCI6MSwiZW1haWwiOiJtYUBhLmNvbSJ9.Qw_3zbpNv24y7gc1BTmoU6o75k05MfzjhHhYqTRx0kU";
	} else {
		// For that  login use that cached token
		token = localStorage.getItem("token");
	}
	return token;
}





//== Function that return the selected marka, model, type of the vehicle
//== Case publish ad which category is Vehicles
function getTermsForVehicles() {
	var model = "";
	var modelMap = $('#model_ad option:selected').map(function (i, el) {
		model = model + el.id + ",";
		var result = {};
		result[el.id] = $(el).val();
		return result;
	}).get();

	model = model.replace(/,\s*$/, "");

	var type = "";
	var typeMap = $('#type_ad option:selected').map(function (i, el) {
		type = type + el.id + ",";
		var result = {};
		result[el.id] = $(el).val();
		return result;
	}).get();

	type = type.replace(/,\s*$/, "");

	return $("#marka_ad").children(":selected").attr("id") + "," + model + "," + type;

}

var googleapi = {
	authorize: function (options) {
		var deferred = $.Deferred();
		//Build the OAuth consent page URL
		var authUrl = 'https://accounts.google.com/o/oauth2/auth?' + $.param({
			client_id: options.client_id,
			redirect_uri: options.redirect_uri,
			response_type: 'code',
			scope: options.scope
		});

		//Open the OAuth consent page in the InAppBrowser
		var authWindow = cordova.InAppBrowser.open(authUrl, '_system', 'location=yes,toolbar=yes');

		//The recommendation is to use the redirect_uri "urn:ietf:wg:oauth:2.0:oob"
		//which sets the authorization code in the browser's title. However, we can't
		//access the title of the InAppBrowser.
		//
		//Instead, we pass a bogus redirect_uri of "http://localhost", which means the
		//authorization code will get set in the url. We can access the url in the
		//loadstart and loadstop events. So if we bind the loadstart event, we can
		//find the authorization code and close the InAppBrowser after the user
		//has granted us access to their data.
		$(authWindow).on('loadstart', function (e) {
			var url = e.originalEvent.url;
			var code = /\?code=(.+)$/.exec(url);
			var error = /\?error=(.+)$/.exec(url);

			if (code || error) {
				//Always close the browser when match is found
				authWindow.close();
			}

			if (code) {
				//Exchange the authorization code for an access token
				$.post('https://accounts.google.com/o/oauth2/token', {
					code: code[1],
					client_id: options.client_id,
					client_secret: options.client_secret,
					redirect_uri: options.redirect_uri,
					grant_type: 'authorization_code'
				}).done(function (data) {
					deferred.resolve(data);

					$("#loginStatus").html('Name: ' + data.given_name);
				}).fail(function (response) {
					deferred.reject(response.responseJSON);
				});
			} else if (error) {
				//The user denied access to the app
				deferred.reject({
					error: error[1]
				});
			}
		});

		return deferred.promise();
	}
};





//== Google Social Login As Web  (Not Working Now)
//------------------------------------------------------------- 
var accessToken;
var UserData = null;

function callGoogle() {

	//alert('starting');
	googleapi.authorize({
		client_id: '282780806297-qq2gmss0cjvgk3pr53mbr61kaa8k43s1.apps.googleusercontent.com',
		client_secret: '282780806297-qq2gmss0cjvgk3pr53mbr61kaa8k43s1.apps.googleusercontent.com',
		redirect_uri: 'urn:ietf:wg:oauth:2.0:oob',
		scope: 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email'
	}).done(function (data) {
		accessToken = data.access_token;
		// alert(accessToken);
		// $loginStatus.html('Access Token: ' + data.access_token);
		makeToast(data.access_token);
		makeToast(JSON.stringify(data));
		getDataProfile();

	});

}

// This function gets data of user.
function getDataProfile() {
	var term = null;
	//  alert("getting user data="+accessToken);
	$.ajax({
		url: 'https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=' + accessToken,
		type: 'GET',
		data: term,
		dataType: 'json',
		error: function (jqXHR, text_status, strError) {},
		success: function (data) {
			var item;

			console.log(JSON.stringify(data));
			// Save the userprofile data in your localStorage.
			localStorage.gmailLogin = "true";
			localStorage.gmailID = data.id;
			localStorage.gmailEmail = data.email;
			localStorage.gmailFirstName = data.given_name;
			localStorage.gmailLastName = data.family_name;
			localStorage.gmailProfilePicture = data.picture;
			localStorage.gmailGender = data.gender;
		}
	});
	disconnectUser();
}

function disconnectUser() {
	var revokeUrl = 'https://accounts.google.com/o/oauth2/revoke?token=' + accessToken;

	// Perform an asynchronous GET request.
	$.ajax({
		type: 'GET',
		url: revokeUrl,
		async: false,
		contentType: "application/json",
		dataType: 'jsonp',
		success: function (nullResponse) {
			// Do something now that user is disconnected
			// The response is always undefined.
			accessToken = null;
			console.log(JSON.stringify(nullResponse));
			console.log("-----signed out..!!----" + accessToken);
		},
		error: function (e) {
			// Handle the error
			// console.log(e);
			// You could point users to manually disconnect if unsuccessful
			// https://plus.google.com/apps
		}
	});
}



myApp.onPageAfterAnimation('advertisementDetails', function (page) {
	//right before animation from page3 to page2
	if (page.from === 'left') {
		//update here page2
		mainView.router.reloadPage('#advertisementDetails');
	}
});