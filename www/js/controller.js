//var app = angular.module('Main', ngSanitize);
//var app = angular.module('Main', ["ngSanitize"]);
//.module("spike", ["ngAnimate", "ngSanitize"]);

/** Final Injectors */
//var app = angular.module('Main', ['ngRoute','ngResource','ngRateIt','ngMaterial','jkAngularRatingStars','mgcrea.pullToRefresh']);
var app = angular.module('Main', ['ngRoute', 'ngResource', 'ngRateIt', 'jkAngularRatingStars', 'mgcrea.pullToRefresh', 'imageSpinner', 'infinite-scroll', 'angular-whenScrolled', "firebase"]);

//var app = angular.module('Main', ['ngRoute','ngResource','ngRateIt']);
//var app = angular.module('Main', []);




var BASE_URL_ROOT = "http://haraj.voxelvention.com/api";

var halal_parent_id = "1";
var buildings_parent_id = "4";
var asas_parent_id = "7";
var vehicles_parent_id = "6";
var more_parent_id = "16";

var Tax_Brand_id = "2";
var Tax_Model_id = "3";
var Tax_Type_id = "4";

var name_validation = 0;
var full_name_validation = 0;
var email_validation = 0;
var mobile_validation = 0;
var password_validation = 0;
var password_confirm_validation = 0;


var page_counter = 0;

/*  Config Initiation for Routing
     And Managing the page-controller and the controller-controller relation
-------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
// Implementing the config for the routing 
// Provided by the angularjs	
/*
app.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider
     .when('/categories', {
         templateUrl: 'pages/vodafon/categories.html',
         controller: 'LoadCategories'
     })
	 .when('/sub_categories/:id/:title', {
         templateUrl: 'pages/vodafon/sub_categories.html',
         controller: 'LoadSubCategories'
     })
	  .when('/presentations/:id/:title/:sub_title', {
         templateUrl: 'pages/vodafon/presentations.html',
         controller: 'LoadPresentations'
     })
	 .when('/add_service', {
         templateUrl: 'pages/vodafon/add_service.html',
         controller: 'AddService'
     })
	 .when('/add_salesman', {
         templateUrl: 'pages/vodafon/add_salesman.html',
         controller: 'AddSalesMan'
     })
	  .when('/add_category', {
		
         templateUrl: 'pages/vodafon/add_category.html',
         controller: 'LoadCategories'
     })
	  .when('/add_subcategory', {	
         templateUrl: 'pages/vodafon/add_subcategory.html',
         controller: 'LoadCategories'
     })
	 .when('/categories_edit/:id/:title/:description', {
         templateUrl: 'pages/vodafon/categories_edit.html',
         controller: 'CategoryEdit'
     })
	  .when('/sub_categories_edit/:id/:title/:description/:cat_title/:cat_id', {
         templateUrl: 'pages/vodafon/sub_categories_edit.html',
         controller: 'SubCategoryEdit'
     })
	 .when('/presentation_edit/:id/:title/:description/:cat_title/:cat_id/:sub_cat_id', {	
         templateUrl: 'pages/vodafon/presentation_edit.html',
         controller: 'PresentationEdit'
     })
	 
	  .when('/chartjs', {
		
         templateUrl: 'pages/charts/chartjs.html',
         controller: 'LoadSubCategories'
     })
	  .when('/dashboard', {
		
         templateUrl: 'pages/vodafon/dashboard.html',
         controller: 'dashboardController'
     })
	 .when('/login', {
		
         templateUrl: 'pages/vodafon/login.html',
         controller: 'LoadSubCategories'
     })
	  .when('/tracking', {
		
         templateUrl: 'pages/vodafon/tracking.html',
         controller: 'LoadSubCategories'
     })
	   .when('/users', {
		
         templateUrl: 'pages/vodafon/Users.html',
         controller: 'LoadUsers'
     })
	   .when('/upload_apk', {
		
         templateUrl: 'pages/vodafon/upload_apk.html',
         controller: 'LoadSubCategories'
     })
	 
     .otherwise({
         //redirectTo: 'chartjs'
		 //redirectTo: 'categories'
		 redirectTo: 'categories'
     });
}]);
*/


/*
app.config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
}]);
*/


/* 
	                        Categories Controller
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
app.controller("CategoriesCtrl", function ($scope, $rootScope, $http) {

    $scope.goToVehicles = function () {
        $rootScope.$broadcast('vehicleEvent', {
            "id": "1"
        });
    };

    $scope.goToBuildings = function () {
        $rootScope.$broadcast('buildEvent', {
            "id": "2"
        });
    };

    $scope.goToHalal = function () {
        $rootScope.$broadcast('halalEvent', {
            "id": "3"
        });
    };

    $scope.goToMore = function () {
        //localStorage.setItem("cat_id", "4");
        $rootScope.$broadcast('moreEvent', {
            "id": "4"
        });
    };

    $scope.goToAdvertisement = function () {
        //localStorage.setItem("cat_id", "5");
        $rootScope.$broadcast('advEvent', {
            "id": "5"
        });
    };

    $scope.goToAsas = function (id, title) {
        //localStorage.setItem("cat_id", "5");
        $rootScope.$broadcast('asasEvent', {
            "id": id,
            "title": title
        });
    };

    $scope.goToProfile = function () {
        //localStorage.setItem("cat_id", "5");
        $rootScope.$broadcast('profileEvent', {
            "token": getToken()
        });
        goToProfilePage();
    };



    $scope.goToNotification = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('notificationEvent', {
                "token": getToken()
            });
            goToNotificationPage();
        }

    };





});


/* 
                        halal Controller
*------------------------------------------------------------------------------------------------------------------------------------------------------------*
*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
app.controller("halalCtrl", function ($scope, $rootScope, $http) {


    $scope.$on('halalEvent', function (events, args) {

        console.log(args);
        //** Change the URL onlu */
        showProgressBoth();
        $http.get(BASE_URL_ROOT + "/terms?token=" + getToken() + "&parent_id=" + halal_parent_id)
            .then(successCallback, errorCallback);

        function successCallback(response) {
            //success code
            hideProgressBoth();
            $scope.result = [];
            angular.forEach(response.data.data, function (value, key) {
                $scope.result.push(value);
            });
            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }
    })


    $scope.fireProductsEvent = function (id) {
        $rootScope.$broadcast('productsFromHalalEvent', {
            "id": id
        });
    };


    $scope.goToProfile = function () {
        $rootScope.$broadcast('profileEvent', {
            "token": getToken()
        });
        goToProfilePage();
    };


    $scope.goToNotification = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('notificationEvent', {
                "token": getToken()
            });
            goToNotificationPage();
        }

    };

});


/* 
	                        building Controller
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
app.controller("buildingCtrl", function ($scope, $rootScope, $http) {


    $scope.$on('buildEvent', function (events, args) {
        console.log(args);

        //** Change the URL onlu */
        showProgressBoth();
        $http.get(BASE_URL_ROOT + "/terms?token=" + getToken() + "&parent_id=" + buildings_parent_id)
            .then(successCallback, errorCallback);

        function successCallback(response) {
            //success code
            hideProgressBoth();
            $scope.result = [];

            angular.forEach(response.data.data, function (value, key) {
                if (value.parent_id == buildings_parent_id) {
                    $scope.result.push(value);
                }

            });
            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }
    })


    $scope.fireProductsEvent = function (id) {
        $rootScope.$broadcast('productsFromBuildingsEvent', {
            "id": id
        });
    };



    $scope.goToProfile = function () {
        $rootScope.$broadcast('profileEvent', {
            "token": getToken()
        });
        goToProfilePage();
    };


    $scope.goToNotification = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('notificationEvent', {
                "token": getToken()
            });
            goToNotificationPage();
        }

    };

});



/* 
	                        vehicle Controller
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
app.controller("vehicleCtrl", function ($scope, $rootScope, $http) {


    $scope.$on('vehicleEvent', function (events, args) {
        console.log(args);

        showProgressBoth();
        $http.get(BASE_URL_ROOT + "/taxonomies?token=" + getToken() + "&keys=car_brand,car_type,car_model").then(successCallback, errorCallback);

        function successCallback(response) {
            //success code
            hideProgressBoth();
            $scope.result = [];
            $scope.marka = [];
            $scope.type = [];
            $scope.model = [];

            angular.forEach(response.data.data, function (value, key) {

                if (value.key == "car_brand") {
                    angular.forEach(value.terms, function (value, key) {
                        $scope.marka.push(value);
                    });
                } else if (value.key == "car_model") {
                    angular.forEach(value.terms, function (value, key) {
                        $scope.model.push(value);
                    });
                } else if (value.key == "car_type") {
                    angular.forEach(value.terms, function (value, key) {
                        $scope.type.push(value);
                    });
                }
            });

            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }

    })


    $scope.$on('markatEvent', function (events, args) {
        console.log(args);
        $rootScope.$broadcast('getMarkatFromServerEvent', "");
    })

    $scope.fireProductsEvent = function (id) {
        //localStorage.setItem("cat_id", "5");
        $rootScope.$broadcast('productsFromVehiclesEvent', {
            "id": id
        });
    };


    $scope.goToProfile = function () {
        //localStorage.setItem("cat_id", "5");
        $rootScope.$broadcast('profileEvent', {
            "token": getToken()
        });
        goToProfilePage();
    };


    $scope.goToNotification = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('notificationEvent', {
                "token": getToken()
            });
            goToNotificationPage();
        }

    };

});



/* 
	                        advertisement/products Controller
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
app.controller("advCtrl", function ($scope, $rootScope, $http, $q) {

    $scope.onReload = function () {
        // alert('reload');

    };


    /*
        $scope.myPagingFunction = function()
        {
            alert("counter " + page_counter);
            page_counter = page_counter + 1;
        }*/

    $scope.more = function () {
        //alert("ds");
    };
    //$scope.more();//twice execute-> controller:load and scroll:load


    //From Halal
    $scope.$on('productsFromHalalEvent', function (events, args) {
        console.log(args);
        showProgressBoth();
        $http.get(BASE_URL_ROOT + "/posts?" + "token=" + getToken() + "&terms_ids=" + args.id)
            .then(successCallback, errorCallback);

        function successCallback(response) {

            //success code
            hideProgressBoth();
            $scope.result = [];
            $scope.prodcuts = [];

            if (response.data.meta.pagination.count == "0") {
                makeToast("لا توجد اعلانات")
            } else {
                angular.forEach(response.data.data, function (value, key) {
                    $scope.prodcuts.push(value);
                });
            }
            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }
    })


    // From Vehicles
    $scope.$on('productsFromVehiclesEvent', function (events, args) {
        console.log(args);


        var model = "";
        var modelMap = $('#model option:selected').map(function (i, el) {
            model = model + el.id + ",";
            var result = {};
            result[el.id] = $(el).val();
            return result;
        }).get();

        //y = JSON.stringify(y);
        model = model.replace(/,\s*$/, "");
        //alert(model);

        var type = "";
        var typeMap = $('#type option:selected').map(function (i, el) {
            type = type + el.id + ",";
            var result = {};
            result[el.id] = $(el).val();
            return result;
        }).get();

        type = type.replace(/,\s*$/, "");

        showProgressBoth();
        $http.get(BASE_URL_ROOT + "/posts?" +
            "token=" + getToken() +
            "&terms_ids=" + model + "," + type + "," + $("#marka").val() + "&term_key=vehicles"
        ).then(successCallback, errorCallback);

        function successCallback(response) {

            //success code
            hideProgressBoth();

            $scope.result = [];
            $scope.prodcuts = [];

            if (response.data.meta.pagination.count == "0") {
                makeToast("لا توجد اعلانات")
            } else {
                angular.forEach(response.data.data, function (value, key) {
                    //alert("prod " + value.title);
                    $scope.prodcuts.push(value);
                });
            }
            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }
    })



    // From Buildings
    $scope.$on('productsFromBuildingsEvent', function (events, args) {
        console.log(args);

        showProgressBoth();
        $http.get(BASE_URL_ROOT + "/posts?" + "token=" + getToken() + "&terms_ids=" + args.id)
            .then(successCallback, errorCallback);

        function successCallback(response) {


            //success code
            hideProgressBoth();

            $scope.result = [];
            $scope.prodcuts = [];

            if (response.data.meta.pagination.count == "0") {
                makeToast("لا توجد اعلانات")
            } else {

                angular.forEach(response.data.data, function (value, key) {
                    $scope.prodcuts.push(value);

                });
            }
            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }
    })


    // From More
    $scope.$on('moreEvent', function (events, args) {
        //console.log(args);

        //** Change the URL onlu */
        showProgressBoth();

        $http.get(BASE_URL_ROOT + "/posts?token=" + getToken() + "&no_terms=1").then(successCallback, errorCallback);
        //alert(BASE_URL_ROOT+"/products.php");
        function successCallback(response) {
            hideProgressBoth();


            $scope.result = [];
            $scope.prodcuts = [];


            if (response.data.meta.pagination.count == "0") {
                makeToast("لا توجد اعلانات")
            } else {

                angular.forEach(response.data.data, function (value, key) {
                    $scope.prodcuts.push(value);
                });
            }
            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }
    })







    // From Asas
    /* Which get the POSTS directly without catgeories */
    $scope.$on('asasEvent', function (events, args) {

        showProgressBoth();
        $http.get(BASE_URL_ROOT + "/posts?" + "token=" + getToken() + "&terms_ids=" + asas_parent_id).then(successCallback, errorCallback);

        function successCallback(response) {
            //success code
            hideProgressBoth();
            $scope.result = [];
            $scope.prodcuts = [];



            if (response.data.meta.pagination.count == "0") {
                makeToast("لا توجد اعلانات")
            } else {
                angular.forEach(response.data.data, function (value, key) {
                    $scope.prodcuts.push(value);
                });
            }

            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }
    })

    // From Newest Ads
    $scope.$on('advEvent', function (events, args) {
        console.log(args);
        showProgressBoth();

        $http.get(BASE_URL_ROOT + "/posts?token=" + getToken()).then(successCallback, errorCallback);

        function successCallback(response) {

            console.log(response)

            //success code
            hideProgressBoth();
            $scope.result = [];
            $scope.prodcuts = [];

            $scope.current_page = response.data.meta.pagination.current_page
            $scope.total_pages = response.data.meta.pagination.total_pages

            if (response.data.meta.pagination.count == "0") {
                makeToast("لا توجد اعلانات")
            } else {

                angular.forEach(response.data.data, function (value, key) {
                    //alert("prod");
                    $scope.prodcuts.push(value);
                });
            }
            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }
    })


    // from fawzy
    // Attach 'infinite' event handler
    $$('.infinite-scroll').on('infinite', function () {

        //  alert ($scope.current_page)
        if ($scope.current_page < $scope.total_pages) {
            $scope.current_page = $scope.current_page + 1
            showProgressBoth();
            $http.get(BASE_URL_ROOT + "/posts?token=" + getToken() + "&page=" + $scope.current_page).then(successCallback, errorCallback);

            function successCallback(response) {

                console.log(response)

                //success code
                hideProgressBoth();
                $scope.result = [];


                $scope.current_page = response.data.meta.pagination.current_page
                $scope.total_pages = response.data.meta.pagination.total_pages

                if (response.data.meta.pagination.count == "0") {
                    makeToast("لا توجد اعلانات")
                } else {

                    angular.forEach(response.data.data, function (value, key) {
                        //alert("prod");
                        $scope.prodcuts.push(value);
                    });
                }
                $scope.isVisible = function (name) {
                    return true; // return false to hide this artist's albums
                };

            }

            function errorCallback(error) {
                //error code
                makeToast(error.data.message);
                hideProgressBoth();
            }



        }

    });


    // From Search
    $scope.$on('searchEvent', function (events, args) {
        //console.log(args);

        showProgressBoth();

        $http.get(BASE_URL_ROOT + "/posts?token=" + getToken() + "&q=" + $("#main_search").val()).then(successCallback, errorCallback);
        //alert(BASE_URL_ROOT+"/products.php");
        function successCallback(response) {
            //success code
            hideProgressBoth();

            $scope.result = [];
            $scope.prodcuts = [];

            if (response.data.meta.pagination.count == "0") {
                makeToast("لا توجد اعلانات")
            } else {

                angular.forEach(response.data.data, function (value, key) {
                    $scope.prodcuts.push(value);
                });
            }
            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }
    })



    // From adDetails Similar Ads Button
    $scope.$on('productsFromSimilarAdsEvent', function (events, args) {
        console.log(args);

        showProgressBoth();
        $http.get(BASE_URL_ROOT + "/posts?token=" + getToken() + "&sim_post_id=" + args.id).then(successCallback, errorCallback);

        function successCallback(response) {
            //success code
            hideProgressBoth();

            $scope.result = [];
            $scope.prodcuts = [];

            if (response.data.meta.pagination.count == "0") {
                makeToast("لا توجد اعلانات")
            } else {

                angular.forEach(response.data.data, function (value, key) {
                    $scope.prodcuts.push(value);
                });
            }
            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }

    })



    $scope.fireAdDetailsEvent = function (id) {
        localStorage.setItem("ad_details_id", id);
        $rootScope.$broadcast('adDetailsEvent', {
            "id": id
        });
    };



    $scope.goToProfile = function () {
        $rootScope.$broadcast('profileEvent', {
            "token": getToken()
        });
        goToProfilePage();
    };



    $scope.goToNotification = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('notificationEvent', {
                "token": getToken()
            });
            goToNotificationPage();
        }

    };
});


/* 
	                       advertisementDetails/adDetails Controller
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
app.controller("adDetailsCtrl", function ($scope, $rootScope, $http, $q, $timeout) {

    $scope.checked = false;

    $scope.goToEditComment = function () {
        goToEditComment();
    }

    // 5.Fawazy
    $scope.fireChatEvent = function (user_token) {
        // alert("fire chat with " + user_token);
        goToChat();
        $rootScope.$broadcast('chatEvent', {
            "user_token": user_token
        });
    }

    $scope.$on('adDetailsEventFromMyAds', function (events, args) {

    });

    $scope.$on('adDetailsEventFromMyAds', function (events, args) {
        showProgressBoth();

        $http.get(BASE_URL_ROOT + "/posts/" + args.id + "?token=" + getToken())
            .then(successCallback, errorCallback);

        function successCallback(response) {
            //success code
            hideProgressBoth();

            $rootScope.hideit = true;

            $scope.result = [];
            $scope.terms = [];

            $scope.id = response.data.id;

            $scope.imgs = [];
            $scope.comments = [];

            $scope.title = response.data.title;

            $("#text_details").text(response.data.title);

            $scope.description = response.data.description;
            $scope.bookmarked = response.data.is_bookmarked;

            if (response.data.is_bookmarked == 0) {
                $("#add_to_favorite").text("أضف للمفضلة");
            } else if (response.data.is_bookmarked == 1) {
                $("#add_to_favorite").text("حذف من المفضلة");
            }
            $scope.phone = response.data.publisher.phone_number;
            $scope.region = response.data.publisher.region;
            $scope.authorName = response.data.publisher.name;
            $scope.authorImage = response.data.publisher.image;
            $scope.date = response.data.created_at;
            $scope.firebase_token = response.data.publisher.firebase_token;
            //$scope.whatsapp = response.data.whatsapp;
            //$scope.catgeory = response.data.ca;
            //$scope.subcategory = response.data.whatsapp;
            $scope.ratings_count = response.data.ratings_count;

            angular.forEach(response.data.comments.data, function (value, key) {
                $scope.comments.push(value);
                if (value.user.id == localStorage.getItem("id")) {
                    $(".more.link.open-popover").show();
                } else {
                    $(".more.link.open-popover").hide();
                }
            });

            angular.forEach(response.data.images, function (value, key) {
                $scope.imgs.push(value);

            });
            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }
    })


    $scope.$on('adDetailsEvent', function (events, args) {

        clearadvDetailsPage();

        console.log(args);
        console.log(BASE_URL_ROOT + "/posts/" + args.id + "?token=" + getToken());
        showProgressBoth();

        $http.get(BASE_URL_ROOT + "/posts/" + args.id + "?token=" + getToken())
            .then(successCallback, errorCallback);

        function successCallback(response) {
            //success code
            hideProgressBoth();

            $rootScope.hideit = false;


            $scope.result = [];
            $scope.terms = [];

            $scope.id = response.data.id;

            $scope.imgs = [];
            $scope.comments = [];

            $scope.title = response.data.title;

            $("#text_details").text(response.data.title);

            $scope.description = response.data.description;
            $scope.bookmarked = response.data.is_bookmarked;

            if (response.data.is_bookmarked == 0) {
                $("#add_to_favorite").text("أضف للمفضلة");
            } else if (response.data.is_bookmarked == 1) {
                $("#add_to_favorite").text("حذف من المفضلة");
            }
            $scope.phone = response.data.publisher.phone_number;
            $scope.region = response.data.publisher.region;
            $scope.authorName = response.data.publisher.name;
            $scope.authorImage = response.data.publisher.image;
            $scope.date = response.data.created_at;
            $scope.whatsapp = response.data.whatsapp;
            $scope.catgeory = response.data.whatsapp;
            $scope.subcategory = response.data.whatsapp;
            $scope.firebase_token = response.data.publisher.firebase_token;

            $scope.ratings_count = response.data.ratings_count;

            angular.forEach(response.data.comments.data, function (value, key) {
                $scope.comments.push(value);
                if (value.user.id == localStorage.getItem("id")) {
                    $(".more.link.open-popover").show();
                } else {
                    $(".more.link.open-popover").hide();
                }
            });

            angular.forEach(response.data.images, function (value, key) {
                $scope.imgs.push(value);

            });
            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }
    })


    $scope.makeCall = function (num) {
        window.location.href = 'tel:' + num;
    };

    function onSuccess(result) {
        //alert("Success:"+result);
    }

    function onError(result) {
        //alert("Error:"+result);
    }


    $scope.shareMessageToWhatsapp = function (msg) {
        //window.plugins.socialsharing.share('Test Share', null, null, 'http://link');
        //window.plugins.CallNumber.callNumber(onSuccess, onError, "01273265954", false);
        window.plugins.socialsharing.shareViaWhatsApp("", null /* img */ , "حراج" /* url */ , function () {
                console.log('share ok')
            },
            function (errormsg) {
                // alert(errormsg)
            }
        )


    };



    $scope.storeAddEditCommentId = function (id) {
        goToAddComment();
        localStorage.setItem("prod_to_commented", id);
    };

    $scope.fireSimilarAds = function (id) {
        $rootScope.$broadcast('productsFromSimilarAdsEvent', {
            "id": id
        });
    };


    $scope.bookmark = function (id, bookmarked) {

        if (bookmarked == 1) //bookmarked
        {
            var data = {
                "post_id": id
            };

            showProgressBoth();

            $http.delete(BASE_URL_ROOT + "/users/me/favourites/" + id + "?token=" + getToken(), JSON.stringify(data)).then(successCallback, errorCallback);

            function successCallback(response) {
                //success code
                hideProgressBoth();
                makeToast("تمت الإزالة من المفضلة");
                $scope.isVisible = function (name) {
                    return true; // return false to hide this artist's albums
                };

            }

            function errorCallback(error) {
                //error code
                makeToast(error.data.message);
                hideProgressBoth();
            }



        } else {

            var data = {
                "post_id": id
            };

            showProgressBoth();

            $http.post(BASE_URL_ROOT + "/users/me/favourites?token=" + getToken(), JSON.stringify(data)).then(successCallback, errorCallback);

            function successCallback(response) {
                //success code
                hideProgressBoth();
                makeToast("تمت الإضافة للمفضلة");
                $scope.isVisible = function (name) {
                    return true; // return false to hide this artist's albums
                };

            }

            function errorCallback(error) {
                //error code
                makeToast(error.data.message);
                hideProgressBoth();
            }
        }

    };


    $scope.unbookmark = function (id, bookmarked) {
        var data = {
            "post_id": id
        };

        showProgressBoth();

        $http.delete(BASE_URL_ROOT + "/users/me/favourites/" + id + "?token=" + getToken(), JSON.stringify(data)).then(successCallback, errorCallback);

        function successCallback(response) {
            //success code
            hideProgressBoth();
            makeToast("تمت الإزالة من المفضلة");
            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }


    };


    $scope.deleteAd = function (id) {

        $http.delete(BASE_URL_ROOT + "/posts/" + id + "?token=" + getToken()).then(successCallback, errorCallback);
        //$http.get(BASE_URL_ROOT+"/posts/" + id  + "?token=" + getToken()).then(successCallback, errorCallback);

        function successCallback(response) {
            //success code
            makeToast("تم الحذف");
            goTomyAdvertisementPage();
            $rootScope.$broadcast('myadsEvent', {
                "token": getToken()
            });
            $scope.result = response.data.result;

            //alert(response.data.result);

            $scope.success = response.data.success;
            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            //makeToast("فشل الحذف");
            makeToast(error.data.message);
            hideProgressBoth();
        }
    }

    $scope.changeToSold = function (id) {

        //alert("change " + id);
        var data = {
            //"name": $('#name_login').val(),
            "is_sold": "1"
        };

        showProgressBoth();
        $http.post(BASE_URL_ROOT + "/posts/" + id + "?token=" + getToken(), JSON.stringify(data))
            .then(successCallback, errorCallback);

        function successCallback(response) {
            //success code
            hideProgressBoth();
            //makeToast(JSON.stringify(response.data));
            makeToast("تم تغيير الحالة للبيع");


            goTomyAdvertisementPage();
            $rootScope.$broadcast('myadsEvent', {
                "token": getToken()
            });

            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }
    }

    $scope.saveCommentId = function (commentId) {
        //alert("commentId " + commentId);
        localStorage.setItem("commentId", commentId);
    }

    $scope.fireAdEdit = function (id) {
        localStorage.setItem("adId", id);
        goToEditAdvertisement();
        $rootScope.$broadcast('editAdEvent', {
            "id": id
        });
    }



    $scope.goToReport = function (id) {
        localStorage.setItem("report_id", id);
        goToReportPage();
    };


    $scope.goToProfile = function () {
        //localStorage.setItem("cat_id", "5");
        $rootScope.$broadcast('profileEvent', {
            "token": getToken()
        });
        goToProfilePage();
    };

    $scope.model = {
        basic: 0,
        readonly: 2.5,
        readonly_enables: true,
        minMaxStep: 6,
        minMaxStep2: 8.75,
        pristine: 3,
        resetable: 0,
        heightWidth: 1.5,
        callbacks: 0,
        custom: 4,
    };

    $scope.ratedCallback = function () {
        makeToast($scope.model.callbacks + " تقييمك");
        /** Here Post the rate value wuth your id and token */
        var data = {
            //"name": $('#name_login').val(),
            "value": $scope.model.callbacks
        };

        showProgressBoth();
        $http.post(BASE_URL_ROOT + "/posts/" + localStorage.getItem("ad_details_id") + "/ratings?token=" + getToken(), JSON.stringify(data))
            //$http.get(BASE_URL_ROOT)
            .then(successCallback, errorCallback);

        function successCallback(response) {
            //success code
            hideProgressBoth();
            makeToast("تم ارسال تقييمك");



            //$scope.names = response.data;
        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();

        }


    };

    $scope.resetCallback = function () {
        //alert('Reset clicked!');
        console.log('Reset clicked!');
    };


    $scope.confirmRating = function (newRating) {
        var d = $q.defer();

        $timeout(function () {
            //if(confirm('Are you sure about rating us with '+newRating+' stars?')){
            d.resolve();
            //}else{
            d.reject();
            //}
        });

        return d.promise;
    };

    $scope.confirmReset = function () {
        var d = $q.defer();
        // if(confirm('Are you sure about resetting this rating?')){
        d.resolve();
        //}else{
        //    d.reject();
        //}
        return d.promise;
    };


    $scope.followPost = function (id) {
        makeToast("تم متابعة الاعلان");
    }








    $scope.goToProfile = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('profileEvent', {
                "token": getToken()
            });
            goToProfilePage();
        }

    };

    $scope.goToSearch = function () {
        $rootScope.$broadcast('vehicleEvent', {
            "id": "1"
        });
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToSearchPage();
        }

    };

    $scope.goToHome = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToHomePage();
        }

    };

    $scope.goToPublishAd = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('clearUplaodAdEvent', {
                "token": getToken()
            });
            goToPublishAdPage();
        }

    };

    $scope.goToNotification = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('notificationEvent', {
                "token": getToken()
            });
            goToNotificationPage();
        }

    };


});




/* 
	                       advertisementDetails/adDetails Controller
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
app.controller("adDetailsForEditCtrl", function ($scope, $rootScope, $http) {


    $scope.checked = false;

    $scope.$on('editAdEvent', function (events, args) {

        clearadvDetailsPage();

        console.log(args);
        showProgressBoth();

        $http.get(BASE_URL_ROOT + "/posts/" + args.id + "?token=" + getToken())
            .then(successCallback, errorCallback);

        function successCallback(response) {
            //success code
            hideProgressBoth();

            $scope.result = [];
            $scope.terms = [];

            $scope.id = response.data.id;

            $scope.imgs = [];
            $scope.comments = [];

            $scope.title = response.data.title;
            $scope.description = response.data.description;
            $scope.bookmarked = response.data.bookmarked;
            $scope.phone = response.data.publisher.phone_number;
            $scope.region = response.data.publisher.region;
            $scope.authorName = response.data.publisher.name;
            $scope.authorImage = response.data.author_img;
            $scope.date = response.data.created_at;
            $scope.whatsapp = response.data.whatsapp;
            $scope.catgeory = response.data.whatsapp;
            $scope.subcategory = response.data.whatsapp;
            $scope.firebase_token = response.data.publisher.firebase_token;

            $scope.ratings_count = response.data.ratings_count;
            angular.forEach(response.data.comments.data, function (value, key) {
                $scope.comments.push(value);
                if (value.user.id == localStorage.getItem("id")) {
                    $(".more.link.open-popover").show();
                } else {
                    $(".more.link.open-popover").hide();
                }
            });

            angular.forEach(response.data.images, function (value, key) {
                $scope.imgs.push(value);

            });
            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }
    })


    $scope.makeCall = function (num) {
        window.location.href = 'tel:' + num;
    };

    function onSuccess(result) {}

    function onError(result) {}


    $scope.shareMessageToWhatsapp = function (msg) {

        makeToast("share now");
        //window.plugins.socialsharing.share('Test Share', null, null, 'http://link');
        //window.plugins.CallNumber.callNumber(onSuccess, onError, "01273265954", false);
        window.plugins.socialsharing.shareViaWhatsApp("", null /* img */ , "حراج" /* url */ , function () {
                console.log('share ok')
            },
            function (errormsg) {
                // alert(errormsg)
            }
        )


    };



    $scope.storeAddEditCommentId = function (id) {
        //localStorage.setItem("cat_id", "4");
        //alert("adId is  : " + id );
        //$rootScope.$broadcast('addEditCommentEvent', {"id": id});
        // alert("تعديل رد");
        $("#comment_title").text("تعديل رد");
        localStorage.setItem("prod_to_commented", id);
    };

    $scope.fireSimilarAds = function (id) {
        //alert("similar to adId : " + id );
        $rootScope.$broadcast('productsFromSimilarAdsEvent', {
            "id": id
        });
    };


    $scope.bookmark = function (id, bookmarked) {

        var data = {
            "post_id": id
        };

        showProgressBoth();

        $http.post(BASE_URL_ROOT + "/users/me/favourites?token=" + getToken(), JSON.stringify(data)).then(successCallback, errorCallback);

        function successCallback(response) {
            //success code
            hideProgressBoth();

            makeToast("تمت الإضافة للمفضلة");
            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }
        // }

    };


    $scope.unbookmark = function (id, bookmarked) {

        var data = {
            "post_id": id
        };

        showProgressBoth();

        $http.delete(BASE_URL_ROOT + "/users/me/favourites/" + id + "?token=" + getToken(), JSON.stringify(data)).then(successCallback, errorCallback);

        function successCallback(response) {
            //success code
            hideProgressBoth();

            makeToast("تمت الإزالة من المفضلة");
            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }

    };


    $scope.deleteAd = function (id) {

        $http.delete(BASE_URL_ROOT + "/posts/" + id + "?token=" + getToken()).then(successCallback, errorCallback);

        function successCallback(response) {
            //success code
            makeToast("تم الحذف");
            $scope.result = response.data.result;
            $scope.success = response.data.success;
            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }
    }


    /*//This will hide the DIV by default.
           // $scope.IsVisible = false;
            $scope.ShowHide = function () {
                //If DIV is visible it will be hidden and vice versa.
                //$scope.IsVisible = $scope.ShowPassport;
                alert("yes");
            }*/

    $scope.changeToSold = function (id) {

        //alert("change " + id);
        var data = {
            "is_sold": "1"
        };


        makeToast(JSON.stringify(data));
        showProgressBoth();
        $http.post(BASE_URL_ROOT + "/posts/" + id + "?token=" + getToken(), JSON.stringify(data))
            .then(successCallback, errorCallback);

        function successCallback(response) {
            //success code


            hideProgressBoth();
            //makeToast(JSON.stringify(response.data));
            makeToast("تم تغيير الحالة للبيع ");

            goToAdvertisement();

            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }
    }

    $scope.saveCommentId = function (commentId) {
        localStorage.setItem("commentId", commentId);
    }


    $scope.goToProfile = function () {
        $rootScope.$broadcast('profileEvent', {
            "token": getToken()
        });
        goToProfilePage();
    };



    $scope.goToNotification = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('notificationEvent', {
                "token": getToken()
            });
            goToNotificationPage();
        }

    };


});







/* 
	                    editComment    editComment Controller
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
app.controller("commentEditAddCtrl", function ($scope, $rootScope, $http) {

    $scope.addEditComment = function () {
        if (localStorage.getItem("commentId")) //the localstorage have a comment Id so Edit
        {
            makeToast("جارى التعديل");
            var data = {
                "post_id": localStorage.getItem("prod_to_commented"),
                "text": $("#ta_comment").val()
            };

            if ($("#ta_comment").val()) {

                showProgressBoth();
                $http.post(BASE_URL_ROOT + "/comments/" + localStorage.getItem("commentId") + "?token=" + getToken(), JSON.stringify(data))
                    .then(successCallback, errorCallback);

                function successCallback(response) {
                    //success code
                    hideProgressBoth();

                    localStorage.setItem("commentId", "");

                    goToBack();
                    $rootScope.$broadcast('adDetailsEvent', {
                        "id": localStorage.getItem("prod_to_commented")
                    });


                    $("#ta_comment").val('');
                    //goToBack();

                    $scope.isVisible = function (name) {
                        return true; // return false to hide this artist's albums
                    };

                }

                function errorCallback(error) {
                    //error code
                    makeToast(error.data.message);
                    hideProgressBoth();
                }

            } else {
                makeToast("أدخل تعليق");
            }

        } else // add new comment
        {
            makeToast("جارى الإضافة");

            var data = {
                "post_id": localStorage.getItem("prod_to_commented"),
                "text": $("#ta_comment").val()
            };

            if ($("#ta_comment").val()) {

                showProgressBoth();
                $http.post(BASE_URL_ROOT + "/comments?token=" + getToken(), JSON.stringify(data))
                    .then(successCallback, errorCallback);

                function successCallback(response) {
                    //success code
                    hideProgressBoth();

                    goToBack();
                    $rootScope.$broadcast('adDetailsEvent', {
                        "id": localStorage.getItem("prod_to_commented")
                    });

                    $("#ta_comment").val('');

                    $scope.isVisible = function (name) {
                        return true; // return false to hide this artist's albums
                    };

                }

                function errorCallback(error) {
                    //error code
                    makeToast(error.data.message);
                    hideProgressBoth();
                }

            } else {
                makeToast("أدخل تعليق");
            }

        }



    };


    $scope.goToProfile = function () {
        //localStorage.setItem("cat_id", "5");
        $rootScope.$broadcast('profileEvent', {
            "token": getToken()
        });
        goToProfilePage();
    };



    $scope.goToNotification = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('notificationEvent', {
                "token": getToken()
            });
            goToNotificationPage();
        }

    };


});




/* 
	                        addAdvertisementCtrl Controller
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
app.controller("addAdvertisementCtrl", function ($scope, $rootScope, $http) {




    // Handling the main image only
    $scope.uploadImage = function () {
        window.imagePicker.getPictures(
            function (results) {
                localStorage.setItem("images_length", results.length);
                for (var i = 0; i < results.length; i++) {
                    console.log('Image URI url: ' + results[i]);
                    $('#img').attr('src', results[i]);


                    window.resolveLocalFileSystemURI(results[i],
                        function (fileEntry) {
                            // convert to Base64 string
                            fileEntry.file(
                                function (file) {
                                    //got file
                                    var reader = new FileReader();
                                    reader.onloadend = function (evt) {
                                        var imgData = evt.target.result; // this is your Base64 string
                                        attachImage(imgData);
                                        console.log("zzz " + imgData);

                                    };
                                    reader.readAsDataURL(file);
                                },
                                function (evt) {
                                    //failed to get file
                                });
                        },
                        // error callback
                        function () {}
                    );

                    //convertAllImagesAndFillForm(results[i],i);
                }




            },
            function (error) {
                console.log('Error: ' + error);
            }, {
                maximumImagesCount: 1
            }
        );
    }



    // Handling The 10 images
    $scope.uploadImages = function () {

        window.imagePicker.getPictures(
            function (results) {
                localStorage.setItem("images_length", results.length);

                clearCounter();
                for (var i = 0; i < results.length; i++) {
                    console.log('Image URI url: ' + i + results[i]);
                    $('#img' + (i + 1)).attr('src', results[i]);
                    window.resolveLocalFileSystemURI(results[i], function (fileEntry) {
                            // convert to Base64 string
                            fileEntry.file(
                                function (file) {
                                    //got file
                                    console.log("zzz " + i);
                                    var reader = new FileReader();
                                    reader.onloadend = function (evt) {
                                        var imgData = evt.target.result; // this is your Base64 string
                                        attachImages(imgData);
                                        console.log("zzz " + imgData);

                                    };
                                    reader.readAsDataURL(file);
                                },
                                function (evt) {
                                    //failed to get file
                                });
                        },
                        // error callback
                        function () {}
                    );
                }




            },
            function (error) {
                console.log('Error: ' + error);
            }, {
                maximumImagesCount: 10
            }
        );


    }


    $scope.deleteImage = function (imgId) {
        //alert("delete image of id " + imgId + " adId " + localStorage.getItem("adId"));

        var data = {
            "post_id": localStorage.getItem("adId")
        };

        if (imgId == 0) {

            $http.delete(BASE_URL_ROOT + "/posts/" + localStorage.getItem("adId") + "/main_image" + "?token=" + getToken(), JSON.stringify({
                data
            })).then(successCallback, errorCallback);

            function successCallback(response) {
                //success code
                hideProgressBoth();
                makeToast("تم إزالة الصورة الرئيسية");
                $("#img0").attr("src", "");
                $scope.isVisible = function (name) {
                    return true; // return false to hide this artist's albums
                };
                $rootScope.$broadcast('adDetailsEvent', {
                    "id": localStorage.getItem("adId")
                });

            }

            function errorCallback(error) {
                //error code
                makeToast(error.data.message);
                hideProgressBoth();
            }

        } else {
            //alert(BASE_URL_ROOT+"/posts/"+localStorage.getItem("adId")+"/images/"+imgId+"?token=" + getToken());
            $http.delete(BASE_URL_ROOT + "/posts/" + localStorage.getItem("adId") + "/images/" + imgId + "?token=" + getToken(), JSON.stringify(data)).then(successCallback, errorCallback);

            function successCallback(response) {
                //success code
                hideProgressBoth();
                makeToast("تم إزالة الصورة");
                $("#img" + (imgId)).attr("src", "");
                $rootScope.$broadcast('adDetailsEvent', {
                    "id": localStorage.getItem("adId")
                });
                $scope.isVisible = function (name) {
                    return true; // return false to hide this artist's albums
                };

            }

            function errorCallback(error) {
                //error code
                makeToast(error.data.message);
                hideProgressBoth();
            }

        }

    }

    $scope.$on('editAdEvent', function (events, args) {


        $("#text_publish_edit").text("تعديل الإعلان");

        $('.ionicons.ion-android-delete').show();
        $('.ionicons.ion-android-upload').hide();

        clearadvDetailsPage();

        showProgressBoth();


        /** First Request */
        $http.get(BASE_URL_ROOT + "/taxonomies?token=" + getToken() + "&keys=category,car_model,car_type,car_brand")
            .then(successCallback1, errorCallback1)
            .then(makeRequest2);

        function successCallback1(response) {

            hideProgressBoth();

            $scope.result = [];
            $scope.sub = [];
            $scope.model = [];
            $scope.type = [];
            $scope.marka = [];



            angular.forEach(response.data.data, function (value, key) {

                if (value.key == "category") {
                    angular.forEach(value.terms, function (value, key) {
                        if (value.parent_id == null) {
                            $scope.result.push(value);
                        } else {
                            $scope.sub.push(value);
                        }
                    });
                } else if (value.key == "car_brand") {
                    angular.forEach(value.terms, function (value, key) {
                        $scope.marka.push(value);
                    });
                } else if (value.key == "car_type") {
                    angular.forEach(value.terms, function (value, key) {
                        $scope.type.push(value);
                    });
                } else if (value.key == "car_model") {
                    angular.forEach(value.terms, function (value, key) {
                        $scope.model.push(value);
                    });
                }



            });
        }




        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }


        /** Second Request */
        function makeRequest2() {
            showProgressBoth();

            $http.get(BASE_URL_ROOT + "/posts/" + args.id + "?token=" + getToken())
                .then(successCallback, errorCallback).then(triggerTheCatgeory);

            function successCallback(response) {
                //success code
                hideProgressBoth();

                $scope.title = response.data.title;
                $scope.description = response.data.description;

                $("#ad_title").val(response.data.title);
                $("#ad_description").val(response.data.description);

                angular.forEach(response.data.terms, function (value, key) {
                    if (value.parent_id == null) {


                        if (value.id == vehicles_parent_id) {
                            $scope.cat_name = value.title;
                            $scope.cat_id = value.id;

                            $("#select_category_upload_ad").val(value.id);

                            $('#li_marka').show();
                            $('#li_type').show();
                            $('#li_model').show();

                            $('#li_model').attr('disabled', false); //add
                            $('#li_type').attr('disabled', false); //add
                            $('#li_marka').attr('disabled', false); //add

                            $("#select_category_upload_ad").change(function () {
                                // Check input( $( this ).val() ) for validity here
                                //alert("ds :" + $("#select_category_upload_ad").val());
                                //$("#select_category_upload_ad").val();
                            });


                        }

                        if (value.taxonomy_id == Tax_Brand_id) {
                            $scope.brand_name = value.title;
                            $scope.brand_id = value.id;

                            $("#marka_ad").val(value.id);
                        }

                        if (value.taxonomy_id == Tax_Model_id) {
                            $scope.model_name = value.title;
                            $scope.model_id = value.id;

                            $("#model_ad").val(value.id);
                        }
                        if (value.taxonomy_id == Tax_Type_id) {
                            $scope.type_name = value.title;
                            $scope.type_id = value.id;

                            $("#type_ad").val(value.id);


                        }

                        if (value.id == halal_parent_id ||
                            value.id == buildings_parent_id ||
                            value.id == asas_parent_id) {
                            $scope.cat_name = value.title;
                            $scope.cat_id = value.id;

                        }

                        if (value.id == more_parent_id) {
                            $scope.cat_name = value.title;
                            $scope.cat_id = value.id;

                            //alert("6 more" + value.title);
                        }

                    } else {
                        $scope.sub_cat_name = value.title;
                        $scope.sub_cat_id = value.id;

                        //alert("7 sub" + value.title);
                    }


                    $scope.imgs = [];
                    var count = 0;
                    angular.forEach(response.data.images, function (value, key) {
                        $("#img" + (count + 1)).attr("src", value);
                        appendImagesUrls("image" + (count + 1), value);
                        count = count + 1;
                    });

                    count = 0;

                    $("#img").attr("src", response.data.main_image);
                    appendImagesUrls("image", response.data.main_image);

                    $scope.upload_type = "edit";
                });

            }




        }

        function errorCallback1(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }

        function triggerTheCatgeory() {

            $("#select_category_upload_ad").trigger('change');
        }

    })


    $scope.init = function () {

        $('.ionicons.ion-android-delete').hide();
        $('.ionicons.ion-android-upload').show();

        $http.get(BASE_URL_ROOT + "/taxonomies?token=" + getToken() + "&keys=category").then(successCallback, errorCallback);

        function successCallback(response) {
            //success code
            //alert(JSON.stringify(response.data));
            $scope.result = [];
            $scope.sub = [];
            $scope.model = [];
            $scope.type = [];
            $scope.marka = [];


            angular.forEach(response.data.data, function (value, key) {
                angular.forEach(value.terms, function (value, key) {
                    if (value.parent_id == null) {
                        //alert(value.title);
                        $scope.result.push(value);
                    }
                });

            });
            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }
    };



    $scope.$on('clearUplaodAdEvent', function (events, args) {

        $("#text_publish_edit").text("رفع إعلان");
        $("#ad_title").val('');
        $("#ad_description").val('');
        $("#img").attr("src", "");
        $("#img1").attr("src", "");
        $("#img2").attr("src", "");
        $("#img3").attr("src", "");
        $("#img4").attr("src", "");
        $("#img5").attr("src", "");
        $("#img6").attr("src", "");
        $("#img7").attr("src", "");
        $("#img8").attr("src", "");
        $("#img9").attr("src", "");
        $("#img10").attr("src", "");

        $('.ionicons.ion-android-delete').hide();
        $('.ionicons.ion-android-upload').show();

        localStorage.setItem("adId", "");

        //Here Same As Init
        $http.get(BASE_URL_ROOT + "/taxonomies?token=" + getToken() + "&keys=category").then(successCallback, errorCallback);

        function successCallback(response) {
            //success code
            //alert(JSON.stringify(response.data));
            $scope.result = [];
            $scope.sub = [];
            $scope.model = [];
            $scope.type = [];
            $scope.marka = [];


            angular.forEach(response.data.data, function (value, key) {
                angular.forEach(value.terms, function (value, key) {
                    if (value.parent_id == null) {
                        //alert(value.title);
                        $scope.result.push(value);
                    }
                });

            });
            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }

    });

    $scope.getSubCat = function () {

        if ($("#select_category_upload_ad").children(":selected").attr("id") != null) {
            //Hanlding for Vehicle case
            if ($("#select_category_upload_ad").children(":selected").attr("id") == vehicles_parent_id) {

                //makeToast("vehicles");
                $('#li_marka').show();
                $('#li_type').show();
                $('#li_model').show();

                $('#li_model').attr('disabled', false); //add
                $('#li_type').attr('disabled', false); //add
                $('#li_marka').attr('disabled', false); //add

                //$http.get(BASE_URL_ROOT+"/terms?token=" + getToken() ).then(successCallback, errorCallback);
                $http.get(BASE_URL_ROOT + "/taxonomies?token=" + getToken() + "&keys=category,car_brand,car_type,car_model").then(successCallback, errorCallback);

                function successCallback(response) {
                    //success code
                    $scope.result = [];

                    $scope.sub = [];

                    angular.forEach(response.data.data, function (value, key) {


                        if (value.key == "category") {
                            angular.forEach(value.terms, function (value, key) {
                                if (value.parent_id == null) {
                                    $scope.result.push(value);
                                }


                                if (value.parent_id == $("#select_category_upload_ad").children(":selected").attr("id")) {
                                    $scope.sub.push(value);
                                }
                            });
                        }

                        if (value.key == "car_brand") {
                            angular.forEach(value.terms, function (value, key) {
                                $scope.marka.push(value);
                            });
                        }
                        if (value.key == "car_model") {
                            angular.forEach(value.terms, function (value, key) {
                                $scope.model.push(value);
                            });
                        }
                        if (value.key == "car_type") {
                            angular.forEach(value.terms, function (value, key) {
                                $scope.type.push(value);
                            });
                        }



                    });
                }

            }
            //Handling For Any Other Case
            else {


                $('#li_marka').hide();
                $('#li_type').hide();
                $('#li_model').hide();

                $('#li_model').attr('disabled', true); //add
                $('#li_type').attr('disabled', true); //add
                $('#li_marka').attr('disabled', true); //add

                $http.get(BASE_URL_ROOT + "/terms?token=" + getToken()).then(successCallback, errorCallback);
                //$http.get(BASE_URL_ROOT+"/taxonomies?token=" + getToken() + "&keys=category,car_brand,car_type,car_model" ).then(successCallback, errorCallback);

                function successCallback(response) {
                    //success code
                    //alert(JSON.stringify(response.data));
                    //$scope.names = response.data;
                    //$scope.names = {"result":[{"id":"1","name":"مواليد \/ اطفال","icon":"http:\/\/192.232.214.91\/~awsalnyapp\/uploads\/category\/1496674483_baby.png"},{"id":"3","name":"إلكترونيات","icon":"http:\/\/192.232.214.91\/~awsalnyapp\/uploads\/category\/9.png"},{"id":"5","name":"تجهيزات الرجل","icon":"http:\/\/192.232.214.91\/~awsalnyapp\/uploads\/category\/81.png"},{"id":"7","name":"التخفيضات","icon":"http:\/\/192.232.214.91\/~awsalnyapp\/uploads\/category\/1496669699_1-10.png"},{"id":"8","name":"المطاعم والكوفيهات","icon":"http:\/\/192.232.214.91\/~awsalnyapp\/uploads\/category\/118.png"},{"id":"32","name":"سيارات","icon":"http:\/\/192.232.214.91\/~awsalnyapp\/uploads\/category\/20120523_161139.jpg"}],"success":true};

                    $scope.result = [];

                    $scope.sub = [];

                    angular.forEach(response.data.data, function (value, key) {


                        if (value.parent_id == null) {
                            $scope.result.push(value);
                        }


                        if (value.parent_id == $("#select_category_upload_ad").children(":selected").attr("id")) {
                            //alert(value.title);
                            $scope.sub.push(value);
                        }

                    });
                }

            }


            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }
    }

    $scope.uploadAd = function () {
        //alert("upload ad");


        /*
        var fd = new FormData();
        fd.append('file', file);
        fd.append('data', 'string');
        $http.post(uploadUrl, fd, {
        transformRequest: angular.identity,
        headers: {'Content-Type': undefined}
        })
        .success(function(){
        })
        .error(function(){
        });
        */

        //$rootScope.$broadcast('productsFromHalalEvent', {"id": id});
        /*$http.get(BASE_URL_ROOT+"/subCategoriesForAdPublish.php").then(successCallback, errorCallback);

            function successCallback(response){
                //success code
                alert(JSON.stringify(response.data));
                //$scope.names = response.data;
                //$scope.names = {"result":[{"id":"1","name":"مواليد \/ اطفال","icon":"http:\/\/192.232.214.91\/~awsalnyapp\/uploads\/category\/1496674483_baby.png"},{"id":"3","name":"إلكترونيات","icon":"http:\/\/192.232.214.91\/~awsalnyapp\/uploads\/category\/9.png"},{"id":"5","name":"تجهيزات الرجل","icon":"http:\/\/192.232.214.91\/~awsalnyapp\/uploads\/category\/81.png"},{"id":"7","name":"التخفيضات","icon":"http:\/\/192.232.214.91\/~awsalnyapp\/uploads\/category\/1496669699_1-10.png"},{"id":"8","name":"المطاعم والكوفيهات","icon":"http:\/\/192.232.214.91\/~awsalnyapp\/uploads\/category\/118.png"},{"id":"32","name":"سيارات","icon":"http:\/\/192.232.214.91\/~awsalnyapp\/uploads\/category\/20120523_161139.jpg"}],"success":true};

                $scope.result = [];
                
                angular.forEach(response.data.result, function(value, key) {
                    $scope.result.push(value);
                });
                $scope.isVisible = function(name){
                    return true;// return false to hide this artist's albums
                };

            }
            function errorCallback(error){
                //error code
            }*/
    };




    $scope.goToProfile = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('clearUplaodAdEvent', {
                "token": getToken()
            });
            $rootScope.$broadcast('profileEvent', {
                "token": getToken()
            });
            goToProfilePage();
        }

    };

    $scope.goToSearch = function () {
        $rootScope.$broadcast('vehicleEvent', {
            "id": "1"
        });
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('clearUplaodAdEvent', {
                "token": getToken()
            });
            goToSearchPage();
        }

    };

    $scope.goToHome = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('clearUplaodAdEvent', {
                "token": getToken()
            });
            goToHomePage();
        }

    };

    $scope.goToPublishAd = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('clearUplaodAdEvent', {
                "token": getToken()
            });
            goToPublishAdPage();
        }

    };

    $scope.goToNotification = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('clearUplaodAdEvent', {
                "token": getToken()
            });
            $rootScope.$broadcast('notificationEvent', {
                "token": getToken()
            });

            goToNotificationPage();
        }

    };




});




/* 
	                        signup Controller
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
//app.controller("signupCtrl", function($scope,$rootScope,$http) {
app.controller("verifyCtrl", function ($scope, $rootScope, $http) {


    //$scope.$on('register', function(events, args){
    $scope.register = function () {

        if ($('#verification_code').val() && $('#verification_password').val()) {


            var data = {
                "email": localStorage.getItem("email"),
                "email_verification_code": $('#verification_code').val()
            };


            //alert(JSON.stringify(data));

            //alert(BASE_URL_ROOT+"/auth-tokens");
            showProgressBoth();

            $http.post(BASE_URL_ROOT + "/auth-tokens", JSON.stringify(data))
                //$http.get(BASE_URL_ROOT)
                .then(successCallback, errorCallback);

            function successCallback(response) {
                //success code
                //makeToast(JSON.stringify(response.data));
                //alert(JSON.stringify(JSON.stringify(response.data)));
                //alert("user id:" + response.data.user.id);
                //alert("user token:" + response.data.token);

                hideProgressBoth();


                var data2 = {
                    "password": $('#verification_password').val(),
                    "email_verification_code": $('#verification_code').val()
                };

                //alert(JSON.stringify(data2));
                //alert(BASE_URL_ROOT+"/users/"+response.data.user.id+"?token="+response.data.token);
                showProgressBoth();


                $http.post(BASE_URL_ROOT + "/users/" + response.data.user.id + "?token=" + response.data.token, JSON.stringify(data2))
                    //$http.get(BASE_URL_ROOT)
                    .then(successCallback, errorCallback);

                function successCallback(response) {

                    hideProgressBoth();

                    changeToLogin();

                    $('#verification_code').val('');
                    $('#verification_password').val('');

                    makeToast("اهلا بك");
                }

                function errorCallback(error) {
                    //error code
                    hideProgressBoth();
                    makeToast(JSON.stringify(error.data.message));

                }






            }

            function errorCallback(error) {
                //error code
                makeToast(error.data.message);
                hideProgressBoth();
            }


        } else {
            makeToast("نأكد من ملأ جميع الحقول");
        }



    }

    $scope.resend = function () {

        var data = {
            "email": localStorage.getItem("email")
        };


        //alert(JSON.stringify(data));
        showProgressBoth();

        $http.post(BASE_URL_ROOT + "/email-verification-codes", JSON.stringify(data))
            //$http.get(BASE_URL_ROOT)
            .then(successCallback, errorCallback);

        function successCallback(response) {
            //success code
            //makeToast(JSON.stringify(response.data));
            //alert(JSON.stringify(JSON.stringify(response.data)));
            //alert("email:" + response.data.email);

            hideProgressBoth();

            changeToVerfication();

            $('#email_ver').val('');
            //$('#verification_password').val('');

            //makeToast("اهلا بك");

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }
    }

});



/* 
	                        forgot Controller
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
app.controller('forgotCtrl', function ($scope, $rootScope, $http) {


    $scope.sendVerCode = function () {
        if ($('#email_ver').val()) {

            localStorage.setItem("email", $('#email_ver').val());

            var data = {
                "email": $('#email_ver').val()
            };


            //alert(JSON.stringify(data));
            showProgressBoth();

            $http.post(BASE_URL_ROOT + "/email-verification-codes", JSON.stringify(data))
                //$http.get(BASE_URL_ROOT)
                .then(successCallback, errorCallback);

            function successCallback(response) {
                //success code
                //makeToast(JSON.stringify(response.data));
                //alert(JSON.stringify(JSON.stringify(response.data)));
                //alert("email:" + response.data.email);

                hideProgressBoth();

                changeToVerfication();

                $('#email_ver').val('');
                //$('#verification_password').val('');

                //makeToast("اهلا بك");

            }

            function errorCallback(error) {
                //error code
                makeToast(error.data.message);
                hideProgressBoth();
            }





        } else {
            makeToast("نأكد من ملأ جميع الحقول");
        }



    }


});
/* 
	                        signup Controller
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
//app.controller("verifyCtrl", function($scope,$rootScope,$http) {
app.controller("signupCtrl", function ($scope, $rootScope, $http) {

    $scope.register = function () {

        if ($('#signup_check').is(':checked')) {

            if ($('#name').val() && $('#full_name').val() && $('#email').val() && $('#password').val() && $('#password_confirm').val() && $('#mobile').val()) {


                if ($('#name').val()) {
                    //alert($('#name').val());
                    if ($('#name').val().indexOf(' ') >= 0) {
                        $("#name_container").css('border', "1px solid red");
                        document.getElementById("error_name").innerHTML = "*" + "لا يجب إدخال مسافات فارغة";
                        name_validation = 0;
                    } else {
                        $("#name_container").css('border', "");
                        document.getElementById("error_name").innerHTML = "";
                        name_validation = 1;
                    }


                } else {
                    //alert($('#name').val());
                    $("#name_container").css('border', "1px solid red");
                    document.getElementById("error_name").innerHTML = "*" + "يجب إدخال اسم";
                    name_validation = 0;
                }

                if ($('#full_name').val()) {

                    $("#full_name_container").css('border', "");
                    document.getElementById("error_full_name").innerHTML = "";
                    full_name_validation = 1;

                } else {
                    $("#full_name_container").css('border', "1px solid red");
                    document.getElementById("error_full_name").innerHTML = "*" + "يجب إدخال اسم";
                    full_name_validation = 0;

                }

                if ($('#email').val()) {
                    $("#email_container").css('border', "");
                    document.getElementById("error_email").innerHTML = "";
                    email_validation = 1;

                } else {
                    $("#email_container").css('border', "1px solid red");
                    document.getElementById("error_email").innerHTML = "*" + "يجب إدخال بريد الكتروني";
                    email_validation = 0;
                }
                if ($('#password').val()) {
                    $("#password_container").css('border', "");
                    document.getElementById("error_password").innerHTML = "";
                    password_validation = 1;
                } else {
                    $("#password_container").css('border', "1px solid red");
                    document.getElementById("error_password").innerHTML = "*" + "يجب ادخال كلمة سر";
                    password_validation = 0;

                }
                if ($('#mobile').val()) {
                    //alert($('#mobile').val().toString().length);
                    if ($('#mobile').val().toString().length != 11) {
                        $("#mobile_container").css('border', "1px solid red");
                        document.getElementById("error_mobile").innerHTML = "*" + "يجب إدخال 11 رقم للجوال";
                        mobile_validation = 0;
                    } else {
                        $("#mobile_container").css('border', "");
                        document.getElementById("error_mobile").innerHTML = "";
                        mobile_validation = 1;
                    }

                } else {
                    $("#mobile_container").css('border', "1px solid red");
                    document.getElementById("error_mobile").innerHTML = "*" + "يجب إدخال 11 رقم للجوال";
                    mobile_validation = 0;
                }



                if ($('#password').val() == $('#password_confirm').val()) {




                    if (name_validation == 1 && full_name_validation == 1 &&
                        email_validation == 1 && mobile_validation == 1 &&
                        password_validation == 1) {
                        var data = {
                            "password": $('#password').val(),
                            "name": $('#name').val(),
                            //"full_name": $('#full_name').val(),
                            "email": $('#email').val(),
                            "phone_number": $('#mobile').val()
                        };


                        //alert(JSON.stringify(data));

                        showProgressBoth();

                        $http.post(BASE_URL_ROOT + "/users", JSON.stringify(data))
                            //$http.get(BASE_URL_ROOT)
                            .then(successCallback, errorCallback);

                        function successCallback(response) {
                            //success code
                            hideProgressBoth();
                            //alert(JSON.stringify(response.data));


                            localStorage.setItem("name", response.data.name);
                            localStorage.setItem("email", response.data.email);
                            localStorage.setItem("mobile", response.data.phone_number);
                            localStorage.setItem("id", response.data.id);
                            localStorage.setItem("image", response.data.image);

                            if (typeof response.data.name === 'undefined') {
                                makeToast("تأكد من بياناتك");
                                $scope.page = "#";
                            } else {


                                changeToVerfication();


                                $('#name').val('');
                                $('#email').val('');
                                $('#password').val('');
                                $('#mobile').val('');
                                $('#full_name').val('');
                                $('#password_confirm').val('');

                                $("#name_container").css('border', "");
                                document.getElementById("error_name").innerHTML = "";
                                $("#email_container").css('border', "");
                                document.getElementById("error_email").innerHTML = "";
                                $("#mobile_container").css('border', "");
                                document.getElementById("error_mobile").innerHTML = "";
                                $("#password_container").css('border', "");
                                document.getElementById("error_password").innerHTML = "";
                                //location.reload();
                            }

                        }

                        function errorCallback(error) {
                            hideProgressBoth();
                            //alert(JSON.stringify(error));
                            //error code
                            angular.forEach(error.data.errors.email, function (value, key) {
                                $("#email_container").css('border', "1px solid red");
                                document.getElementById("error_email").innerHTML = "*" + value;

                            });

                            angular.forEach(error.data.errors.name, function (value, key) {
                                makeToast(value);
                                document.getElementById("error_name").innerHTML = "*" + value;
                            });

                            angular.forEach(error.data.errors.password, function (value, key) {
                                makeToast(value);
                                document.getElementById("error_password").innerHTML = "*" + value;
                            });
                            //makeToast(error.data.message);

                        }

                    } else {
                        makeToast("راجع البيانات جيدا");
                    }




                } else {
                    makeToast("كلمتين السر غير متطابقتين");
                }


            } else {

                if ($('#name').val()) {
                    //alert($('#name').val());
                    if ($('#name').val().indexOf(' ') >= 0) {
                        $("#name_container").css('border', "1px solid red");
                        document.getElementById("error_name").innerHTML = "*" + "لا يجب إدخال مسافات فارغة";
                        name_validation = 0;
                    } else {
                        $("#name_container").css('border', "");
                        document.getElementById("error_name").innerHTML = "";
                        name_validation = 1;
                    }


                } else {
                    //alert($('#name').val());
                    $("#name_container").css('border', "1px solid red");
                    document.getElementById("error_name").innerHTML = "*" + "يجب إدخال اسم";
                    name_validation = 0;
                }

                if ($('#full_name').val()) {

                    $("#full_name_container").css('border', "");
                    document.getElementById("error_full_name").innerHTML = "";
                    full_name_validation = 1;

                } else {
                    $("#full_name_container").css('border', "1px solid red");
                    document.getElementById("error_full_name").innerHTML = "*" + "يجب إدخال اسم";
                    full_name_validation = 0;

                }

                if ($('#email').val()) {
                    $("#email_container").css('border', "");
                    document.getElementById("error_email").innerHTML = "";
                    email_validation = 1;

                } else {
                    $("#email_container").css('border', "1px solid red");
                    document.getElementById("error_email").innerHTML = "*" + "يجب إدخال بريد الكتروني";
                    email_validation = 0;
                }
                if ($('#password').val()) {
                    $("#password_container").css('border', "");
                    document.getElementById("error_password").innerHTML = "";
                    password_validation = 1;
                } else {
                    $("#password_container").css('border', "1px solid red");
                    document.getElementById("error_password").innerHTML = "*" + "يجب ادخال كلمة سر";
                    password_validation = 0;

                }
                if ($('#mobile').val()) {
                    //alert($('#mobile').val().toString().length);
                    if ($('#mobile').val().toString().length != 11) {
                        $("#mobile_container").css('border', "1px solid red");
                        document.getElementById("error_mobile").innerHTML = "*" + "يجب إدخال 11 رقم للجوال";
                        mobile_validation = 0;
                    } else {
                        $("#mobile_container").css('border', "");
                        document.getElementById("error_mobile").innerHTML = "";
                        mobile_validation = 1;
                    }

                } else {
                    $("#mobile_container").css('border', "1px solid red");
                    document.getElementById("error_mobile").innerHTML = "*" + "يجب إدخال 11 رقم للجوال";
                    mobile_validation = 0;
                }





            }
        } //End Of Terms
        else {
            makeToast("من فضلك وافق علي المعاهدة");
        }

    }


    $scope.goToTerms = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {

        } else {

        }
    }

});




/* 
	                        signup Controller
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
app.controller("signinCtrl", function ($scope, $rootScope, $http, $firebaseObject, $firebaseArray, $firebaseAuth) {

    $scope.signin = function () {

        if ($('#mobile_login').val() && $('#password_login').val()) {
            var data = {
                //"name": $('#name_login').val(),
                "name": localStorage.getItem("name"),
                //"email": $('#email').val(),
                "email": $('#mobile_login').val(),
                "password": $('#password_login').val(),
                "phone_number": $('#mobile_login').val()
            };

            showProgressBoth();
            $http.post(BASE_URL_ROOT + "/auth-tokens", JSON.stringify(data))
                .then(successCallback, errorCallback);

            function successCallback(response) {
                //success code
                hideProgressBoth();

                if (typeof response.data.token === 'undefined') {
                    makeToast("تأكد من بياناتك");

                } else {
                    makeToast("مرحبا بك يا " + response.data.user.name);

                    localStorage.setItem("token", response.data.token);
                    localStorage.setItem("password", $('#password_login').val());
                    localStorage.setItem("id", response.data.user.id);
                    localStorage.setItem("email", response.data.user.email);
                    localStorage.setItem("mobile", response.data.user.phone_number);
                    localStorage.setItem("name", response.data.user.name);
                    localStorage.setItem("image", response.data.user.image);
                    localStorage.setItem("firebase_token", response.data.user.firebase_token);

                    // alert(response.data.user.firebase_token);
                    // var auth = ;
                    $scope.firebaseloged = localStorage.getItem('firebaseLoged');

                    $scope.firebase_token = localStorage.getItem('firebase_token');

                    //$scope.message =""
                    console.log("$scope.firebaseloged :", $scope.firebaseloged)
                    console.log("$scope.firebase_token :", $scope.firebase_token)

                    //1.Fawazy
                    if ($scope.firebaseloged == null && $scope.firebase_token == "") {

                        // alert("not logged fire");
                        // alert(localStorage.getItem("firebase_token"));

                        $firebaseAuth().$createUserWithEmailAndPassword(localStorage.getItem("email"), localStorage.getItem("password"))
                            .then(function (firebaseUser) {
                                // alert("User " + firebaseUser.uid + " created successfully!");
                                localStorage.setItem('firebase_token', firebaseUser.uid);
                                $scope.uid = firebaseUser.uid;
                                localStorage.setItem('firebase_token', firebaseUser.uid);
                                $scope.firebaseloged = localStorage.setItem('firebaseLoged', true);

                                // alert("set users")
                                var ref = firebase.database().ref().child("/users/" + firebaseUser.uid);
                                var obj = $firebaseObject(ref);
                                obj.email = localStorage.getItem("email");
                                obj.$save().then(function (ref) {
                                    console.log(ref)
                                    // ref.key === obj.$id; // true
                                }, function (error) {
                                    console.log("Error:", error);
                                });
                                console.log($firebaseObject(firebase.database().ref().child("/users/")))
                                // alert("end users")

                                // $firebaseArray(ref).$add({
                                //     users: firebaseUser.uid
                                // });
                                // alert("end users")


                                // TODO : update user with firbase token to get it again when chatting  *2
                                $rootScope.$broadcast('userUpdate', {
                                    "token": getToken()
                                });
                                changeToCategory();
                                //set publisher firbase token
                                //$scope.openChat($scope.secondUser.firbaseToken)
                            })
                            .catch(function (error) {
                                // alert("Error: ", error);
                            });

                    } else {

                        // alert("logged in fire");
                        // alert(localStorage.getItem("firebase_token"));

                        //2.Fawzy
                        $firebaseAuth().$signInWithEmailAndPassword(localStorage.getItem("email"), localStorage.getItem("password"))
                            .then(function (firebaseUser) {
                                // alert("Signed in as:", firebaseUser.uid);
                                localStorage.setItem('uid', firebaseUser.uid);
                                $scope.uid = firebaseUser.uid;
                                localStorage.setItem('firebase_token', firebaseUser.uid);
                                //
                                console.log("$scope.firebaseloged :", $scope.firebaseloged)

                                // alert("set users")
                                var ref = firebase.database().ref().child("/users/" + firebaseUser.uid);
                                var obj = $firebaseObject(ref);
                                obj.email = localStorage.getItem("email");
                                obj.$save().then(function (ref) {
                                    console.log(ref)
                                    // ref.key === obj.$id; // true
                                }, function (error) {
                                    console.log("Error:", error);
                                });
                                console.log($firebaseObject(firebase.database().ref().child("/users/")))
                                // alert("end users")

                                // $firebaseArray(ref).$add({
                                //     users: firebaseUser.uid
                                // });
                                // alert("end users")


                                // TODO : update user with firbase token to get it again when chatting  *1
                                //set publisher firbase token
                                //$scope.openChat($scope.secondUser.firbaseToken)
                                changeToCategory();
                            })
                            .catch(function (error) {
                                // alert("Authentication failed:", error);

                            });
                    }



                    $('#mobile_login').val('');
                    $('#password_login').val('');

                    $("#email_login_container").css('border', "");
                    $("#password_login_container").css('border', "");
                    document.getElementById("error_email_login").innerHTML = "";
                    document.getElementById("error_password_login").innerHTML = "";
                }

            }

            function errorCallback(error) {

                //error code
                hideProgressBoth();

                document.getElementById("error_email_login").innerHTML = "";
                document.getElementById("error_password_login").innerHTML = "";

                $("#email_login_container").css('border', "");
                $("#password_login_container").css('border', "");

                makeToast(error.data.message);

                angular.forEach(error.data.errors.email, function (value, key) {
                    $("#email_login_container").css('border', "1px solid red");
                    document.getElementById("error_email_login").innerHTML = "*" + value;

                });

                angular.forEach(error.data.errors.password, function (value, key) {
                    $("#password_login_container").css('border', "1px solid red");
                    document.getElementById("error_password_login").innerHTML = "*" + value;

                });

            }
        } else {
            if ($('#mobile_login').val()) {
                $("#email_login_container").css('border', "");
                document.getElementById("error_email_login").innerHTML = "";

            } else {
                $("#email_login_container").css('border', "1px solid red");
                document.getElementById("error_email_login").innerHTML = "*" + "يجب إدخال البريد الالكترونى";
            }
            if ($('#password_login').val()) {
                $("#password_login_container").css('border', "");
                document.getElementById("error_password_login").innerHTML = "";

            } else {
                $("#password_login_container").css('border', "1px solid red");
                document.getElementById("error_password_login").innerHTML = "*" + "يجب إدخال كلمة السر";
            }
        }



    }


    $scope.registerFacebook = function () {
        // makeToast("start facebook token");

        var fbLoginSuccess = function (userData) {
            facebookConnectPlugin.getAccessToken(function (token) {
                var data = {
                    //"name": $('#name_login').val(),
                    "social_provider": "facebook",
                    //"email": $('#email').val(),
                    "provider_token": token
                };

                document.getElementById("error_email_login").innerHTML = token;

                showProgressBoth();
                $http.post(BASE_URL_ROOT + "/auth-tokens", JSON.stringify(data))
                    .then(successCallback, errorCallback);

                function successCallback(response) {
                    //success code
                    console.log(JSON.stringify(response.data));
                    console.log(token);

                    hideProgressBoth();


                    if (typeof response.data.token === 'undefined') {
                        makeToast("تأكد من بياناتك");

                    } else {
                        makeToast("مرحبا بك يا " + response.data.user.name);

                        localStorage.setItem("token", response.data.token);
                        localStorage.setItem("id", response.data.user.id);
                        localStorage.setItem("email", response.data.user.email);
                        localStorage.setItem("mobile", response.data.user.phone_number);
                        localStorage.setItem("name", response.data.user.name);
                        localStorage.setItem("image", response.data.user.image);

                        changeToCategory();

                        $('#mobile_login').val('');
                        $('#password_login').val('');
                    }

                }

                function errorCallback(error) {
                    //error code
                    // makeToast(JSON.stringify(error.data));
                    hideProgressBoth();

                }


            });
        }

        facebookConnectPlugin.login(["public_profile", "email"], fbLoginSuccess,
            function (error) {
                //console.error(error);
                // makeToast("Error " + error);
            }
        );

        /*$socialAuthCordova.loginFacebook(authConfig, customConfig, customURL).then(function (access_token) {
            makeToast(access_token);
        }, function (error) {
            makeToast(error);
        });
        */

    }


    $scope.registerGoogle = function () {
        /*TwitterConnect.login(
        function(result) {
            makeToast('Successful login!');
            makeToast(result);
        }, function(error) {
            makeToast('Error logging in');
            makeToast(error);
        }
        );*/

        // makeToast("start g+ token");

        //callGoogle();


        window.plugins.googleplus.login({
                'scopes': '', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
                'webClientId': '282780806297-qq2gmss0cjvgk3pr53mbr61kaa8k43s1.apps.googleusercontent.com', // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
                'offline': true, // optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode, which can be used to grant offline access to a non-Google server
            },
            function (obj) {
                // makeToast(JSON.stringify(obj)); // do something useful instead of alerting
            },
            function (msg) {
                // makeToast('error: ' + msg);
            }
        );




    }

});






/** Saerch Controller 
 * ================================================================================== */
app.controller('searchCtrl', function ($scope, $rootScope, $http) {


    var fd = new FormData();

    $scope.fireSearchEvent = function () {
        $rootScope.$broadcast('searchEvent', {});
        $("#main_search").val('');
    }

    $scope.goToProfile = function () {
        $rootScope.$broadcast('profileEvent', {
            "token": getToken()
        });
        goToProfilePage();
    };
    $scope.fireVehicleEvent = function () {
        $rootScope.$broadcast('vehicleEvent', {
            "id": "1"
        });
        goToVehiclesSearch();
    };



    $scope.goToNotification = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('notificationEvent', {
                "token": getToken()
            });
            goToNotificationPage();
        }

    };



});




/** profile Controller 
 * ========================================================================================= */
app.controller('profileCtrl', function ($scope, $rootScope, $http) {

    $scope.$on('profileEvent', function (events, args) {
        console.log(args);

        showProgressBoth();
        $http.get(BASE_URL_ROOT + "/users/" + localStorage.getItem("id") + "?token=" + getToken())
            .then(successCallback, errorCallback);

        function successCallback(response) {
            hideProgressBoth();

            $scope.result = [];
            $scope.id = response.data.id;
            $scope.name = response.data.name;
            $scope.email = response.data.email;
            $scope.image = response.data.image;
            $scope.phone_number = response.data.phone_number;
            $scope.region = response.data.region;


            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }
    })


    $scope.goToProfileEdit = function () {
        goToProfileEditPage();
        $rootScope.$broadcast('profileEditEvent', {
            "token": getToken()
        });

    };


    $scope.goToFavourite = function () {
        goToFavouritePage();
        $rootScope.$broadcast('favouriteEvent', {
            "token": getToken()
        });

    };

    $scope.goTomyAdvertisement = function () {
        goTomyAdvertisementPage();
        $rootScope.$broadcast('myadsEvent', {
            "token": getToken()
        });

    };

    $scope.goToMessages = function () {
        goToMessagesPage();
    };


    $scope.goToSignIn = function () {
        localStorage.setItem("token", "");
        localStorage.setItem("name", "");
        localStorage.setItem("id", "");

        changeToLogin();

    };



    $scope.goToProfile = function () {
        $rootScope.$broadcast('profileEvent', {
            "token": getToken()
        });
        goToProfilePage();
    };



    $scope.goToNotification = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('notificationEvent', {
                "token": getToken()
            });
            goToNotificationPage();
        }

    };

});



/** editProfile Controllers 
 * ===================================================================================== */
app.controller('profileEditCtrl', function ($scope, $rootScope, $http) {

    $scope.$on('profileEditEvent', function (events, args) {
        console.log(args);

        showProgressBoth();
        $http.get(BASE_URL_ROOT + "/users/" + localStorage.getItem("id") + "?token=" + getToken())
            .then(successCallback, errorCallback).then(getCities);

        function successCallback(response) {
            hideProgressBoth();

            $scope.result = [];
            $scope.id = response.data.id;
            $scope.name = response.data.name;
            $scope.email = response.data.email;
            $scope.image = response.data.image;
            $scope.phone_number = response.data.phone_number;
            $scope.region = response.data.region;

            $("#profile_edit_name").val(response.data.name);
            $("#profile_edit_email").val(response.data.email);
            $("#profile_edit_phone_number").val(response.data.phone_number);

            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }

        function getCities() {

            $http.get(BASE_URL_ROOT + "/cities?token=" + getToken())
                .then(successCallback2, errorCallback2);

            function successCallback2(response) {
                hideProgressBoth();
                //success code
                $scope.cities = [];

                angular.forEach(response.data.data, function (value, key) {
                    $scope.cities.push(value);
                });

                $scope.isVisible = function (name) {
                    return true; // return false to hide this artist's albums
                };

            }

            function errorCallback2(error) {
                //error code
                makeToast(error.data.message);
                hideProgressBoth();
            }
        }
    })


    //4.Fawzy
    $scope.$on('userUpdate', function (events, args) {

        var fd = new FormData();

        fd.append("name", localStorage.getItem("name"));
        fd.append("email", localStorage.getItem("email"));
        fd.append("firebase_token", localStorage.getItem("firebase_token"));

        showProgressBoth();

        $http.post(BASE_URL_ROOT + "/users/" + localStorage.getItem("id") + "?token=" + getToken(),
                fd, {
                    withCredentials: true,
                    headers: {
                        'Content-Type': undefined
                    },
                    transformRequest: angular.identity
                })


            .then(successCallback, errorCallback);

        function successCallback(response) {
            hideProgressBoth();
            //success code
            $scope.result = [];
            $scope.id = response.data.id;
            $scope.name = response.data.name;
            $scope.email = response.data.email;
            $scope.image = response.data.image;
            $scope.phone_number = response.data.phone_number;
            $scope.region = response.data.region;

            makeToast("تم الحصول علي توكن");

            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.message);
            hideProgressBoth();
        }


    });


    $scope.editProfile = function () {


        var fd = new FormData();


        fd.append("image", getImageUser());
        fd.append("name", $("#profile_edit_name").val());
        fd.append("email", $("#profile_edit_email").val());
        fd.append("phone_number", $("#profile_edit_phone_number").val());
        fd.append("city_id", $("#city_ad").children(":selected").attr("id"));

        showProgressBoth();

        if ($("#profile_edit_name").val() == "") {
            makeToast("please enter your name");
        } else if ($("#profile_edit_phone_number").val() == "") {
            makeToast("please enter your phone number");
        }





        $http.post(BASE_URL_ROOT + "/users/" + localStorage.getItem("id") + "?token=" + getToken(),
                fd, {
                    withCredentials: true,
                    headers: {
                        'Content-Type': undefined
                    },
                    transformRequest: angular.identity
                })


            .then(successCallback, errorCallback);

        function successCallback(response) {
            hideProgressBoth();
            //success code
            $scope.result = [];
            $scope.id = response.data.id;
            $scope.name = response.data.name;
            $scope.email = response.data.email;
            $scope.image = response.data.image;
            $scope.phone_number = response.data.phone_number;
            $scope.region = response.data.region;

            window.localstorage.setItem('mobile', $scope.phone_number)
            window.localstorage.setItem('name', $scope.name)
            // window.localstorage.setItem('email', $scope.email)

            makeToast("تم تغيير البيانات بنجاح");
            $rootScope.$broadcast('profileEvent', {
                "token": getToken()
            });
            goToProfilePage();

            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            console.log(error)
            //error code
            makeToast(error.message);
            hideProgressBoth();
        }

    }


    $scope.uploadFile = function () {
        fd.append('image', getImageUser);
    }


    $scope.goToProfile = function () {
        $rootScope.$broadcast('profileEvent', {
            "token": getToken()
        });
        goToProfilePage();

    };




    $scope.goToNotification = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('notificationEvent', {
                "token": getToken()
            });
            goToNotificationPage();
        }

    };


});





/** favourites Controllers 
 * ===================================================================================== */
app.controller('favouriteCtrl', function ($scope, $rootScope, $http) {

    $scope.$on('favouriteEvent', function (events, args) {
        console.log(args);

        showProgressBoth();

        $http.get(BASE_URL_ROOT + "/users/me/favourites?token=" + getToken()).then(successCallback, errorCallback);

        function successCallback(response) {
            //success code
            hideProgressBoth();

            $scope.result = [];
            $scope.prodcuts = [];

            angular.forEach(response.data.data, function (value, key) {
                //alert(value.title);
                $scope.prodcuts.push(value);
            });
            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }
    });

    $scope.fireAdDetailsEvent = function (id) {
        $rootScope.$broadcast('adDetailsEvent', {
            "id": id
        });
    }


    $scope.goToProfile = function () {
        $rootScope.$broadcast('profileEvent', {
            "token": getToken()
        });
        goToProfilePage();
    };


    $scope.goToNotification = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('notificationEvent', {
                "token": getToken()
            });
            goToNotificationPage();
        }

    };


});



/** myads Controllers 
 * ================================================================================= */
app.controller('myadsCtrl', function ($scope, $rootScope, $http) {

    $scope.onReload = function () {
        // alert('reload');

    };

    $scope.$on('myadsEvent', function (events, args) {
        console.log(args);

        showProgressBoth();

        $http.get(BASE_URL_ROOT + "/users/me/posts?token=" + getToken()).then(successCallback, errorCallback);

        function successCallback(response) {
            //success code
            hideProgressBoth();

            $scope.result = [];
            $scope.prodcuts = [];

            angular.forEach(response.data.data, function (value, key) {
                $scope.prodcuts.push(value);
            });
            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }
    });

    $scope.fireAdDetailsEvent = function (id) {
        $rootScope.$broadcast('adDetailsEventFromMyAds', {
            "id": id
        });
    }






    $scope.goToProfile = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('profileEvent', {
                "token": getToken()
            });
            goToProfilePage();
        }

    };

    $scope.goToSearch = function () {
        $rootScope.$broadcast('vehicleEvent', {
            "id": "1"
        });
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToSearchPage();
        }

    };

    $scope.goToHome = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToHomePage();
        }

    };

    $scope.goToPublishAd = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToPublishAdPage();
        }

    };

    $scope.goToNotification = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('notificationEvent', {
                "token": getToken()
            });
            goToNotificationPage();
        }

    };

});

app.directive('myRepeatDirective', function () {
    return function (scope, element, attrs) {
        //angular.element(element).css('color','blue');
        if (scope.$last) {
            // window.alert("im the last!");
        }
    };
})



/** commission Controllers 
 =============================================================================================*/
app.controller('commissionCtrl', function ($scope, $rootScope, $http) {

    $scope.goToProfile = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('profileEvent', {
                "token": getToken()
            });
            goToProfilePage();
        }

    };

    $scope.goToSearch = function () {
        $rootScope.$broadcast('vehicleEvent', {
            "id": "1"
        });
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToSearchPage();
        }

    };

    $scope.goToHome = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToHomePage();
        }

    };

    $scope.goToPublishAd = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToPublishAdPage();
        }

    };

    $scope.goToNotification = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('notificationEvent', {
                "token": getToken()
            });
            goToNotificationPage();
        }

    };


});

/** bankAccount Controllers
 ================================================================================================= */
app.controller('bankAccountCtrl', function ($scope, $rootScope, $http) {

    $scope.goToProfile = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('profileEvent', {
                "token": getToken()
            });
            goToProfilePage();
        }

    };

    $scope.goToSearch = function () {
        $rootScope.$broadcast('vehicleEvent', {
            "id": "1"
        });
        //localStorage.setItem("cat_id", "5");
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToSearchPage();
        }

    };

    $scope.goToHome = function () {
        //localStorage.setItem("cat_id", "5");
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToHomePage();
        }

    };

    $scope.goToPublishAd = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToPublishAdPage();
        }

    };

    $scope.goToNotification = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('notificationEvent', {
                "token": getToken()
            });
            goToNotificationPage();
        }

    };


});

/** bankAccount Controllers */
app.controller('bankAccountCtrl', function ($scope, $rootScope, $http) {
    $scope.goToProfile = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('profileEvent', {
                "token": getToken()
            });
            goToProfilePage();
        }

    };

    $scope.goToSearch = function () {
        $rootScope.$broadcast('vehicleEvent', {
            "id": "1"
        });
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToSearchPage();
        }

    };

    $scope.goToHome = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToHomePage();
        }

    };

    $scope.goToPublishAd = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToPublishAdPage();
        }

    };

    $scope.goToNotification = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('notificationEvent', {
                "token": getToken()
            });
            goToNotificationPage();
        }

    };
});

/** terms Controllers */
app.controller('termsCtrl', function ($scope, $rootScope, $http) {

    $scope.goToProfile = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('profileEvent', {
                "token": getToken()
            });
            goToProfilePage();
        }

    };

    $scope.goToSearch = function () {

        $rootScope.$broadcast('vehicleEvent', {
            "id": "1"
        });
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToSearchPage();
        }

    };

    $scope.goToHome = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToHomePage();
        }

    };

    $scope.goToPublishAd = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToPublishAdPage();
        }

    };

    $scope.goToNotification = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('notificationEvent', {
                "token": getToken()
            });
            goToNotificationPage();
        }

    };


});

/** blockAds Controllers */
app.controller('blockAdsCtrl', function ($scope, $rootScope, $http) {

    $scope.goToProfile = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('profileEvent', {
                "token": getToken()
            });
            goToProfilePage();
        }

    };

    $scope.goToSearch = function () {
        $rootScope.$broadcast('vehicleEvent', {
            "id": "1"
        });
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToSearchPage();
        }

    };

    $scope.goToHome = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToHomePage();
        }

    };

    $scope.goToPublishAd = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToPublishAdPage();
        }

    };

    $scope.goToNotification = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('notificationEvent', {
                "token": getToken()
            });
            goToNotificationPage();
        }

    };


});

/** notification Controllers */
app.controller('notificationCtrl', function ($scope, $rootScope, $http) {

    $scope.$on('notificationEvent', function (events, args) {

        showProgressBoth();

        $http.get(BASE_URL_ROOT + "/users/me/notifications?token=" + getToken()).then(successCallback, errorCallback);

        function successCallback(response) {
            //success code
            hideProgressBoth();

            $scope.result = [];

            angular.forEach(response.data.data, function (value, key) {
                $scope.result.push(value);
            });

            $scope.isVisible = function (name) {
                return true; // return false to hide this artist's albums
            };

        }

        function errorCallback(error) {
            //error code
            makeToast(error.data.message);
            hideProgressBoth();
        }
        // }

    });


    $scope.goToProfile = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('profileEvent', {
                "token": getToken()
            });
            goToProfilePage();
        }

    };

    $scope.goToSearch = function () {
        $rootScope.$broadcast('vehicleEvent', {
            "id": "1"
        });
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToSearchPage();
        }

    };

    $scope.goToHome = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToHomePage();
        }

    };

    $scope.goToPublishAd = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToPublishAdPage();
        }

    };

});

/** messages Controllers */
app.controller('messagesCtrl', function ($scope, $rootScope, $http, $firebaseObject, $firebaseArray, $firebaseAuth, $q) {

    $scope.uid = localStorage.getItem("firebase_token");


    $scope.members = $firebaseArray(firebase.database().ref().child('/users/' + $scope.uid + '/chats'))

    console.log($scope.members)

    $scope.fireChatEvent = function (user_token) {
        // alert("fire chat with " + user_token);
        goToChat();
        $rootScope.$broadcast('chatEvent', {
            "user_token": user_token
        });
    }

    $scope.goToProfile = function () {
        $rootScope.$broadcast('profileEvent', {
            "token": getToken()
        });
        goToProfilePage();
    };


});

/** contact Controllers */
app.controller('contactCtrl', function ($scope, $rootScope, $http) {

    $scope.contactUs = function () {

        if ($('#contact_name').val() && $('#contact_mobile').val() && $('#contact_mail').val() &&
            $('#contact_message').val()) {
            var data = {
                //"name": $('#name_login').val(),
                "name": $('#contact_name').val(),
                //"email": $('#email').val(),
                "email": $('#contact_mail').val(),
                "phone_number": $('#contact_mobile').val(),
                "text": $('#contact_message').val()
            };

            showProgressBoth();
            $http.post(BASE_URL_ROOT + "/contact-messages", JSON.stringify(data))
                .then(successCallback, errorCallback);

            function successCallback(response) {
                //success code
                //makeToast(JSON.stringify(response.data));
                hideProgressBoth();

                makeToast("شكرا لتواصلك معنا");

                $('#contact_name').val('');
                $('#contact_mobile').val('');
                $('#contact_mail').val('');
                $('#contact_message').val('')

            }

            function errorCallback(error) {
                //error code
                makeToast(error.data.errors.text);
                hideProgressBoth();

            }
        } else {
            makeToast("تأكد من إدخال البيانات");
        }
    };

    $scope.goToProfile = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('profileEvent', {
                "token": getToken()
            });
            goToProfilePage();
        }

    };

    $scope.goToSearch = function () {
        $rootScope.$broadcast('vehicleEvent', {
            "id": "1"
        });
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToSearchPage();
        }

    };

    $scope.goToHome = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToHomePage();
        }

    };

    $scope.goToPublishAd = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToPublishAdPage();
        }

    };

    $scope.goToNotification = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('notificationEvent', {
                "token": getToken()
            });
            goToNotificationPage();
        }

    };

});

/** report Controllers */
app.controller('reportCtrl', function ($scope, $rootScope, $http) {

    $scope.report = function () {

        if ($('#report_name').val() && $('#report_name2').val() &&
            $('#report_message').val()) {
            var data = {
                //"name": $('#name_login').val(),
                "name": $('#report_name').val(),
                //"email": $('#email').val(),
                "name2": $('#report_name2').val(),
                "text": $('#report_message').val()
            };
            showProgressBoth();
            $http.post(BASE_URL_ROOT + "/report-person", JSON.stringify(data))
                .then(successCallback, errorCallback);

            function successCallback(response) {
                //success code
                hideProgressBoth();

                makeToast("شكرا لتواصلك معنا");
                $('#report_name').val('');
                $('#report_name2').val('');
                $('#report_message').val('');
                // $('#contact_message').val('');

                goToBack();
            }

            function errorCallback(error) {
                //error code
                makeToast(error.data.errors.text);
                hideProgressBoth();

            }
        } else {
            makeToast("تأكد من إدخال البيانات");
        }
    };


    $scope.goToProfile = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('profileEvent', {
                "token": getToken()
            });
            goToProfilePage();
        }

    };


    $scope.cancelreport = function () {
        goToBack();
    }


    $scope.goToSearch = function () {
        $rootScope.$broadcast('vehicleEvent', {
            "id": "1"
        });
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToSearchPage();
        }

    };

    $scope.goToHome = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToHomePage();
        }

    };

    $scope.goToPublishAd = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToPublishAdPage();
        }

    };

    $scope.goToNotification = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('notificationEvent', {
                "token": getToken()
            });
            goToNotificationPage();
        }

    };

});



/** report Controllers */
app.controller('reportAd', function ($scope, $rootScope, $http) {

    $scope.report = function () {
        //localStorage.setItem("cat_id", "5");
        var bad = "0";
        var price = "0";

        if ($('#report_check_bad').is(':checked')) {
            bad = "1";
        }

        if ($('#report_check_price').is(':checked')) {
            price = "1";
        }

        if ($('#report_ad_message').val()) {
            var data = {
                //"name": $('#name_login').val(),
                "post_id": localStorage.getItem("report_id"),
                //"email": $('#email').val(),
                "text": $('#report_ad_message').val(),
                "is_insuitable": bad,
                "is_lowering_price": price
            };

            showProgressBoth();
            $http.post(BASE_URL_ROOT + "/reports?token=" + getToken(), JSON.stringify(data))
                .then(successCallback, errorCallback);

            function successCallback(response) {
                //success code
                hideProgressBoth();

                makeToast("تم إبلاغنا سيتم إتخاذ اللازم");
            }

            function errorCallback(error) {
                //error code
                makeToast(error.data.errors.text);
                hideProgressBoth();

            }
        } else {
            makeToast("تأكد من إدخال البيانات");
        }
    };


    $scope.goToProfile = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('profileEvent', {
                "token": getToken()
            });
            goToProfilePage();
        }

    };

    $scope.goToSearch = function () {
        $rootScope.$broadcast('vehicleEvent', {
            "id": "1"
        });
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToSearchPage();
        }

    };

    $scope.goToHome = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToHomePage();
        }

    };

    $scope.goToPublishAd = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToPublishAdPage();
        }

    };


    $scope.goToNotification = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('notificationEvent', {
                "token": getToken()
            });
            goToNotificationPage();
        }

    };


});




/** changePasswordCtrl Controllers */
app.controller('changePasswordCtrl', function ($scope, $rootScope, $http) {

    $scope.changePassword = function () {

        if ($("#old_password").val() == null || $("#new_password").val() == null || $("#new_password_confirm").val() == null ||
            $("#old_password").val() == "" || $("#new_password").val() == "" || $("#new_password_confirm").val() == "") {
            makeToast("تأكد من إكمال البيانات");
        } else {
            if ($("#new_password").val() == $("#new_password_confirm").val()) {
                var data = {
                    "password": $("#new_password").val(),
                    "old_password": $("#old_password").val()
                };


                //alert(BASE_URL_ROOT+"/users/" + localStorage.getItem("id")+ "?token=" + getToken());
                showProgressBoth();
                $http.post(BASE_URL_ROOT + "/users/" + localStorage.getItem("id") + "?token=" + getToken(), JSON.stringify(data))
                    .then(successCallback, errorCallback);

                function successCallback(response) {
                    //success code
                    hideProgressBoth();

                    makeToast("تم تغيير كلمة المرور");

                    $("#old_password").val('');
                    $("#new_password").val('');
                    $("#new_password_confirm").val('');

                    changeToLogin();

                    $('#mobile_login').val('');
                    $('#password_login').val('');
                }

                function errorCallback(error) {
                    //error code
                    makeToast("خطأ " + error.data.message);
                    hideProgressBoth();

                }

            } else {
                makeToast("تأكد من تطابق كلمتين السر");
            }

        }


    }

    $scope.goToProfile = function () {
        //localStorage.setItem("cat_id", "5");
        $rootScope.$broadcast('profileEvent', {
            "token": getToken()
        });
        goToProfilePage();
    };
});






/** Chat Controllers */
app.controller('chatCtrl', function ($scope, $firebaseObject, $firebaseArray, $firebaseAuth, $q) {


    $scope.getChatRef = function (uid, interlocutor) {
        let firstRef = firebase.database().ref().child('/chats/' + uid + ',' + interlocutor);
        let defer = $q.defer();


        firstRef.once("value")
            .then(function (snapshot) {
                var a = snapshot.exists();
                // firstRef.$loaded().then(function() {
                // let a = firstRef.$value
                // alert(a)
                console.log("firstRef", firstRef)
                console.log("a", a)
                if (a) {
                    defer.resolve('/chats/' + uid + ',' + interlocutor);
                } else {
                    // let secondRef = $firebaseObject(firebase.database().ref().child('/chats/' + interlocutor + ',' + uid));
                    let secondRef = firebase.database().ref().child('/chats/' + interlocutor + ',' + uid);
                    secondRef.once("value")
                        .then(function (snapshot) {
                            var b = snapshot.exists();
                            console.log(b)
                            if (!b) {


                                var endpoint1 = $firebaseArray(firebase.database().ref().child('/users/' + uid + '/chats/'));
                                endpoint1.$add(interlocutor)

                                var endpoint2 = $firebaseArray(firebase.database().ref().child('/users/' + interlocutor + '/chats/'));
                                endpoint2.$add(uid)

                                // alert(endpoint1)
                                // console.log(endpoint1)
                                // alert(endpoint2)
                                // console.log(endpoint2)
                                // var endpoint_1 = firebase.database().ref('/users/' + uid + '/chats/' + interlocutor).push();
                                // // var userchats = endpoint_1.child();
                                // $firebaseObject(endpoint_1).$exists().then(function() {
                                //     alert('Profile saved!');
                                // }).catch(function(error) {
                                //     alert('Error!');
                                // });

                                // let endpoint2 = $firebaseObject(firebase.database().ref().child('/users/' + interlocutor + '/chats/' + uid));
                                // endpoint2.$save().then(function() {
                                //     alert('endpoint 2');
                                // }).catch(function(error) {
                                //     alert('Error endpoint 2');
                                // });

                            }
                        });
                    defer.resolve('/chats/' + interlocutor + ',' + uid);
                }
            });


        return defer.promise;
    }




    $scope.$on('chatEvent', function (events, args) {
        $scope.withUser = args.user_token
        $scope.uid = localStorage.getItem("firebase_token");
        // alert("me " + localStorage.getItem("firebase_token"));
        // alert("with " + args.user_token);

        console.log($scope.uid + ',' + $scope.withUser)

        $scope.getChatRef($scope.uid, $scope.withUser).then(data => {

            console.log(data);
            $scope.chats = $firebaseArray(firebase.database().ref().child(data))
            console.log($scope.chats)
        });


    });



    $scope.sendMessage = function () {
        $scope.chats.$add({
            from: localStorage.getItem("firebase_token"),
            message: $("#chat_message").val(),
            type: 'message',
            time: new Date(),
        });
        $scope.message = "";

    };


    $scope.goToProfile = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('profileEvent', {
                "token": getToken()
            });
            goToProfilePage();
        }

    };

    $scope.goToSearch = function () {
        $rootScope.$broadcast('vehicleEvent', {
            "id": "1"
        });
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToSearchPage();
        }

    };

    $scope.goToHome = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToHomePage();
        }

    };

    $scope.goToPublishAd = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            goToPublishAdPage();
        }

    };

    $scope.goToNotification = function () {
        if (localStorage.getItem("id") == "" || localStorage.getItem("id") == null) {
            makeToast("يجب تسجيل الدخول");
        } else {
            $rootScope.$broadcast('notificationEvent', {
                "token": getToken()
            });
            goToNotificationPage();
        }

    };

});


/* 
	                        markat vehicle Controller
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*
	*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*app.controller("markatVehicleCtrl", function($scope,$rootScope,$http) {
    

        $scope.callParentFireEventMarkat = function() {
            //localStorage.setItem("cat_id", "2");
            $rootScope.$broadcast('markatEvent', "");
            
        };

        $scope.$on('getMarkatFromServerEvent', function(events, args){
            console.log(args);
            
            //** Change the URL onlu 
            //alert("ID: " + BASE_URL_ROOT+"/"+args.id);
            
            $http.get(BASE_URL_ROOT).then(successCallback, errorCallback);

                function successCallback(response){
                    //success code
                    //alert(JSON.stringify(response.data));
                    $scope.names = response.data;
                }
                function errorCallback(error){
                    //error code
                }

        })
      
    });
    */








/** Testing Controllers */
app.controller('ImportantController', function ($scope) {
    $scope.data = {
        response: '{"id":"123456","post_id":"12345"}'
    };

    // alert($scope.data);
    // alert($scope.data.response);

    // alert($scope.data.response.id);

    $scope.temp = angular.fromJson($scope.data.response);

    // alert($scope.temp.id);
});