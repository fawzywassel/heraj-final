// build html for messages
function buildMessageHTML(messageText, messageSenderId, messageDateSent, attachmentFileId, messageId, userLogin, status){
  var messageAttach;
  if(attachmentFileId){
      messageAttach = '<img src="' + QB.content.publicUrl(attachmentFileId) + '/' + '/download.xml?token='+token+'" alt="attachment" class="attachments img-responsive" />';
  }

	var isMessageSticker = stickerpipe.isSticker(messageText);

  var delivered = '<img class="icon-small" src="images/delivered.jpg" alt="" id="delivered_'+messageId+'">';
  var read = '<img class="icon-small" src="images/read.jpg" alt="" id="read_'+messageId+'">';

	var messageTextHtml = messageText;
	if (messageAttach) {
		messageTextHtml = messageAttach;
	} else if (isMessageSticker) {
		messageTextHtml = '<div class="message-sticker-container"></div>';

		stickerpipe.parseStickerFromText(messageText, function(sticker, isAsync) {
			if (isAsync) {
				$('#' + messageId + ' .message-sticker-container').html(sticker.html);
			} else {
				messageTextHtml = sticker.html;
			}
		});
	}

	
		var messageHtml;
		
			//alert(userLogin);
			//alert("message_sender_id: " + messageSenderId);
  
  
		  if(messageSenderId == "mahmouddean")
		  {
				  //alert("me");
				  /*messageHtml =
								'<div class="message message-sent" style="background:#fd8e22" id="'+messageId+'">'+
									'<time datetime="'+messageDateSent+ '" class="pull-right">'
										+jQuery.timeago(messageDateSent)+
									'</time>'+

									'<h4 class="list-group-item-heading">'+messageSenderId+'</h4>'+
									'<p class="list-group-item-text">'+
										messageTextHtml +
									'</p>'
									+delivered+read+
								'</div>'
								+
								'<div style="height=100px;"></div>';*/
								
								/*messageHtml = /*'<div align="center" class="messages-date">'+messageDateSent+'</span></div>'
								'<div align="center" class="messages-date">'+'</span></div>'+
								'<div align="right" class="message-text" style="background:#fd8e22">'
                                +messageTextHtml+
                                '<div class="message-date">'+jQuery.timeago(messageDateSent)+'</div>';
								*/
								
								messageHtml = '<div align="right" class="message-name">'+messageSenderId+'</div>'
								+'<div align="right" class="talk-bubble"><div class="talktext">'
								+'<p style="color:#fff; " align="right"'
								+'font-family: \'Cairo-Regular\', sans-serif !important;>'
								+messageTextHtml+'</p>'
								+'<p style="color:#fff; " align="right">'+messageDateSent+'</p>'
								+'</div>'
								
								+'</div>'
								+'<p style="color:#fff; " align="center">'+jQuery.timeago(messageDateSent)+'</p>'
								
								/*'<div align="center" class="messages-date">'+'</span></div>'+
								'<div align="right" class="message-text" style="background:#fd8e22">'
                                +messageTextHtml+
                                '<div class="message-date">'+jQuery.timeago(messageDateSent)+'</div>'*/
								
								
		  }
		  else
		  {
					  //alert("you");
					  /*messageHtml =
									'<div class="message message-with-avatar message-received" style="background:#fff" id="'+messageId+'">'+
										'<time datetime="'+messageDateSent+ '" class="pull-right">'
											+jQuery.timeago(messageDateSent)+
										'</time>'+

										'<h4 class="list-group-item-heading">'+messageSenderId+'</h4>'+
										'<p class="list-group-item-text">'+
											messageTextHtml +
										'</p>'
										+delivered+read+
									'</div>';
									*/
							/*messageHtml = '<div class="message-text" style="background:#fff">'
                            +messageTextHtml+
                            '<div class="message-date">'+jQuery.timeago(messageDateSent)+'</div>'
                            +'</div>';*/
							
							/*messageHtml = '<div align="left" style="box-sizing: border-box;border-radius: 2px;padding: 6px 8px;min-width: 48px;font-size: '+'16px;line-height: 1.2;word-break: break-word;'+'color: #333;min-height: 48px;position: relative;" class="message'+'message-with-avatar message-received">'+                          
                            '<div class="message-name">'+messageSenderId+'</div>'+
                            '<div class="message-text" style="background:"#fff";">'+messageTextHtml+'</div>'+
                            '<div style="background-image:url(//placehold.it/60)" class="message-avatar"></div></div>'
							+'</div><div class="messages-date">'+'</span></div>';*/
							/*+'</div><div class="messages-date">'+messageDateSent+'</span></div>';;*/
							
							//alert(jQuery.timeago(messageDateSent));
							messageHtml = 				'<div class="message-name">'+messageSenderId+'</div>'
													+'<div align="left"  class="talk-bubble2 tri-right left-top" >'
														+'<div class="talktext"><p>'+messageTextHtml+'</p><p>'+jQuery.timeago(messageDateSent)+'</p>'+' </div></div>';
														
													+'</div>'
														+'<div class="message-date">'+jQuery.timeago(messageDateSent)+'</div>';
		  }
   /*messageHtml =
				'<div class="list-group-item" style="background:#fff" id="'+messageId+'">'+
					'<time datetime="'+messageDateSent+ '" class="pull-right">'
						+jQuery.timeago(messageDateSent)+
					'</time>'+

					'<h4 class="list-group-item-heading">'+messageSenderId+'</h4>'+
					'<p class="list-group-item-text">'+
						messageTextHtml +
					'</p>'
					+delivered+read+
				'</div>';*/
	  
	  /*if(user == "mahmouddean")
	  {
		 alert(userLogin);
		  
		  messageHtml =
				'<div class="list-group-item" style="background:#fd8e22" id="'+messageId+'">'+
					'<time datetime="'+messageDateSent+ '" class="pull-right">'
						+jQuery.timeago(messageDateSent)+
					'</time>'+

					'<h4 class="list-group-item-heading">'+messageSenderId+'</h4>'+
					'<p class="list-group-item-text">'+
						messageTextHtml +
					'</p>'
					+delivered+read+
				'</div>';
	  }
	  else
	  {
		  messageHtml =
				'<div class="list-group-item" style="background:#fff" id="'+messageId+'">'+
					'<time datetime="'+messageDateSent+ '" class="pull-right">'
						+jQuery.timeago(messageDateSent)+
					'</time>'+

					'<h4 class="list-group-item-heading">'+messageSenderId+'</h4>'+
					'<p class="list-group-item-text">'+
						messageTextHtml +
					'</p>'
					+delivered+read+
				'</div>';
	  }
	  */
  
  return messageHtml;
}

// build html for dialogs
function buildDialogHtml(dialogId, dialogUnreadMessagesCount, dialogIcon, dialogName, dialogLastMessage) {
  var UnreadMessagesCountShow = '<span class="badge">'+dialogUnreadMessagesCount+'</span>';
      UnreadMessagesCountHide = '<span class="badge" style="display: none;">'+dialogUnreadMessagesCount+'</span>';

  var isMessageSticker = stickerpipe.isSticker(dialogLastMessage);

  var dialogHtml =
      '<a href="#" class="list-group-item inactive" id='+'"'+dialogId+'"'+' onclick="triggerDialog('+"'"+dialogId+"'"+')">'+
                   (dialogUnreadMessagesCount === 0 ? UnreadMessagesCountHide : UnreadMessagesCountShow)+
        '<h4 class="list-group-item-heading">'+ dialogIcon+'&nbsp;&nbsp;&nbsp;' +
            '<span>'+dialogName+'</span>' +
        '</h4>'+
        '<p class="list-group-item-text last-message">'+
            (dialogLastMessage === null ?  "" : (isMessageSticker ? 'Sticker' : dialogLastMessage))+
        '</p>'+
      '</a>';
  return dialogHtml;
}

// build html for typing status
function buildTypingUserHtml(userId, userLogin) {
  var typingUserHtml =
      '<div id="'+userId+'_typing" class="list-group-item typing">'+
        '<time class="pull-right">writing now</time>'+
        '<h4 class="list-group-item-heading">'+ userLogin+'</h4>'+
        '<p class="list-group-item-text"> . . . </p>'+
      '</div>';

  return typingUserHtml;
}

// build html for users list
function buildUserHtml(userLogin, userId, isNew) {
  var userHtml = "<a href='#' id='" + userId;
  if(isNew){
    userHtml += "_new'";
  }else{
    userHtml += "'";
  }
  userHtml += " class='col-md-12 col-sm-12 col-xs-12 users_form' onclick='";
  userHtml += "clickToAdd";
  userHtml += "(\"";
  userHtml += userId;
  if(isNew){
    userHtml += "_new";
  }
  userHtml += "\")'>";
  userHtml += userLogin;
  userHtml +="</a>";

  return userHtml;
}
