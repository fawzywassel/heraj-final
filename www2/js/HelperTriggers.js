/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 

var mydb;

if (window.openDatabase) {
    //Create the database the parameters are 1. the database name 2.version number 3. a description 4. the size of the database (in bytes) 1024 x 1024 = 1MB
     mydb = openDatabase("Fav3", "1.0", "Favourite DataBase", 2*1024 * 1024);
    //create the cars table using SQL for the database using a transaction
	mydb.transaction(function (tx) {
	   tx.executeSql('CREATE TABLE IF NOT EXISTS DEP3 (id integer primary key autoincrement, name unique, wp_id)');
	});
	

} 

else {
    alert("WebSQL is not supported by your browser!");
}



var BASE_URL = "http://192.168.0.104/vodafonebackend/";


function isOnline(no,yes){
			var xhr = XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHttp');
			xhr.onload = function(){
				if(yes instanceof Function){
					yes();
				}
			}
			
			xhr.onerror = function(){
				if(no instanceof Function){
					no();
				}
			}
			xhr.open("GET","http://www.google.com",true);
			xhr.send();
		}


					
	var myService;
     //$(document).ready(function(){
		 
/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
						Device Ready Functions  Start
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

	document.addEventListener("deviceready", function(){
			
		    isOnline(
				function(){
					alert("No Internet Signal.");
					navigator.app.exitApp();
				},
				function(){
					//alert("Successfully connected!");
				}
			);

			
		/*window.requestFileSystem  = window.requestFileSystem || window.webkitRequestFileSystem;
		window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);*/
	    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
        window.resolveLocalFileSystemURI("file:///storage/emulated/0/Download", onResolveSuccess, failResovle);

			  
		  
		  /** Handle The First Time to manipulate the Login
				PreAuthentication to prevent login each time
		  **/
		 var applaunchCount = window.localStorage.getItem('launchCount'); 
		 if(applaunchCount)
		 {
			
			//alert("not null count");
			if(applaunchCount == 1)
					{
					    //This is a second time launch, and count = applaunchCount
					   
						 $.mobile.changePage("#page_categories");
					}
			else{
					    //Local storage is not set, hence first time launch. set the local storage item
					    //Do the other stuff related to first time launch
						
						//window.localStorage.setItem('launchCount', 1);
							/** Get the max id for the app version and set in the localStorage 
							-------------------------------------------------------------------------------------------------------**/
							
				
					  
					  
					  
					  //window.localStorage.getItem("update_id")
					}
		
		 }
		 
		 //First Time
		 else
		 {			 
			 //alert("null count");	
			 // Initialize first time to open app to store the application version for first time only !!
			 $.ajax({
										url: BASE_URL+"initializeUpdates/android", // point to server-side PHP script 
										//dataType: 'json',  // what to expect back from the PHP script, if anything
										cache: false,
										contentType: false,
										processData: false,

										//data: form_data,                         
										type: 'GET',
										success: function(php_script_response){
										 
										 
										  //alert($.parseJSON(php_script_response));
										 //alert(php_script_response);
										 var json = $.parseJSON(php_script_response);
										
															 
											//alert("start update");
											/** Code Download And Install Here **/
											alert(json.update_id);
											//downloadAndInstall(json.update_link);
											window.localStorage.setItem("update_id", json.update_id);
											$("#update_icon").css("color", "grey");
										
										 
									}
						});			 
		 }
		

		
		  
		/**_________________________________________________________________________________________________________*/
		/**_________________________________________________________________________________________________________*/
		/**_________________________________________________________________________________________________________*/
		/**_________________________________________________________________________________________________________*/
		 
		  
		  /** Get the Device type to use it in the Push Notification,
		  *   Depending of the type of the device we will use certaion library
		  **/
		  var deviceType = (navigator.userAgent.match(/iPad/i))  == "iPad" ? "iPad" : (navigator.userAgent.match(/iPhone/i))  == "iPhone" ? "iPhone" : (navigator.userAgent.match(/Android/i)) == "Android" ? "Android" : (navigator.userAgent.match(/BlackBerry/i)) == "BlackBerry" ? "BlackBerry" : "null";
		 
		 if(deviceType == "Android")
		  {
			  screen.orientation.lock('portrait');
			  //screen.orientation.lock('portrait');
			  
			  
			  
			 
			  
			  /*-------------------------------------------------------------------------------------------------------
			  Back Burron Handling 
			  --------------------------------------------------------------------------------------------------------*/
			  document.addEventListener("backbutton", onBackKeyDown, false);
				 function onBackKeyDown() {
				
					if($.mobile.activePage.attr('id') == "page_categories")
					{
						
						var r = confirm("Do you want to exit?");
						if (r == true) {          //OK
						
								navigator.app.exitApp();
								
						} 
						else 
						{                   //Cancel
								navigator.app.backHistory();
						}
					}
				}
			  /** LogIn Check each time opeining the app not by the first time check **/
			   $("#loginForm").on("submit",handleLogin);
			   
			  
		   /*------------------------ Search 
		   -----------------------------------------------------------------------------------------------------
		   -----------------------------------------------------------------------------------------------------*/
	      /**  Handling the key press for search to start the search 
		  **/
		  
		  $(".target").on("keypress", function(event){     
       
				if (event.keyCode === 13) {
			
				var submitIcon = $('.searchbox-icon');
				var page_name = $.mobile.activePage.attr('id');
				var inputBox = $('#searchbox-input_' + page_name);
				
				var searchBox = $('.searchbox');
				var isOpen = true;
			
						if(isOpen == false)
						{
							searchBox.addClass('searchbox-open');
							inputBox.focus();
							isOpen = true;
						} 
						else 
						{
							
							 localStorage.setItem("search_para",inputBox.val());
							 if(inputBox.val() != "")
							 {
										 //alert(inputBox.val());
										 localStorage.setItem("search_keyword", inputBox.val());
										 if($.mobile.activePage.attr('id') == "page_categories")
										 {
											  $.mobile.changePage("#page_search_cat");
										 }
										 else if ($.mobile.activePage.attr('id') == "page_sub_categories")
										 {
											   $.mobile.changePage("#page_search_sub_cat");
										 }
										 else if ($.mobile.activePage.attr('id') == "page_presentation")
										 {
											 $.mobile.changePage("#page_search_presentation");
										 }
		 
							 }
							
							searchBox.removeClass('searchbox-open');
							inputBox.focusout();
							isOpen = false;
							$(".searchbox1").css('display', 'none');
						}
         
			
			
			
					event.preventDefault();
				}
			 });

			$(".searchbox-icon1").click(function () {
				$(".searchbox1").css('display', 'block');
			});
			$(".back-icon1").click(function () {
				var submitIcon = $('.searchbox-icon');
				var page_name = $.mobile.activePage.attr('id');
				var inputBox = $('#searchbox-input_' + page_name);
				
				var searchBox = $('.searchbox');
				var isOpen = true;
			
						if(isOpen == false)
						{
							searchBox.addClass('searchbox-open');
							inputBox.focus();
							isOpen = true;
						} 
						else 
						{
							
							 localStorage.setItem("search_para",inputBox.val());
							 if(inputBox.val() != "")
							 {
										 //alert(inputBox.val());
										 localStorage.setItem("search_keyword", inputBox.val());
										 if($.mobile.activePage.attr('id') == "page_categories")
										 {
											  $.mobile.changePage("#page_search_cat");
										 }
										 else if ($.mobile.activePage.attr('id') == "page_sub_categories")
										 {
											   $.mobile.changePage("#page_search_sub_cat");
										 }
										 else if ($.mobile.activePage.attr('id') == "page_presentation")
										 {
											 $.mobile.changePage("#page_search_presentation");
										 }
											 
										
		 
							 }
							
							searchBox.removeClass('searchbox-open');
							inputBox.focusout();
							isOpen = false;
							$(".searchbox1").css('display', 'none');
						}
				$(".searchbox1").css('display', 'none');
				
			});
		  
		  /*------------------------------------------------------------------------------------------------------------------------------------
		  --------------------------------------------------------------------------------------------------------------------------------------
		  --------------------------------------------------------------------------------------------------------------------------------------*/
			   /** Handle Reset Password Form */
			   $("#checkMailForm").on("submit",checkMail);
			  
			  // First Time
			  if(localStorage.getItem("first_or_no") != "yes")
			  {
				  var serviceName = 'com.red_folder.phonegap.plugin.backgroundservice.sample.MyService';
				  var factory = cordova.require('com.red_folder.phonegap.plugin.backgroundservice.BackgroundService');
				  myService = factory.create(serviceName);
				  go();
				  localStorage.setItem("first_or_no", "no");
				  
				  
				   
			  }
			  
			  else
			  {
				  
			  }
			  
			}//End Of Android If
			
			
			
		  
		  else
		  {
			 	
								window.plugin.backgroundMode.enable();
								var myVar = setInterval(myTimer, 900000);

								function myTimer() {
								
									navigator.geolocation.getCurrentPosition(function(position)
										{
											
											// just to show how to access latitute and longitude
											var location = [position.coords.latitude, position.coords.longitude];	
											$.getJSON("http://www.mpp.ae/en/service/mobile_service.php?request=getNearJson&lat=" + position.coords.latitude + "&lng=" + position.coords.longitude,function(result){
												
												$.each(result, function(i, field){		
													if(field.result == "null")
													{
														
													}
													else
													{
													
														window.plugin.notification.local.add({ message: field.result });  //local notification
													}
													
												});
												
											});	
										},
										function(error)
										{
											// error getting GPS coordinates
											//alert('code: ' + error.code + ' with message: ' + error.message + '\n');
										}, 
										{ enableHighAccuracy: true, maximumAge: 3000, timeout: 5000 });
								
								}
							
   
				
					 
			} //End Of IOS If
			
			
			
			
			
	});
		    
/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
						Device Ready Functions  End
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
		   
		/** LOGIN Section 
		------------------------------------------------------------------------------------------------------------------*/
		function checkPreAuth() {
			var form = $("#loginForm");
			if(window.localStorage["username"] != undefined && window.localStorage["password"] != undefined) {
				$("#username", form).val(window.localStorage["username"]);
				$("#password", form).val(window.localStorage["password"]);
				handleLogin();
			}
		}

		function handleLogin() {
			var form = $("#loginForm"); 
			//disable the button so we can't resubmit while we wait
			$("#submitButton",form).attr("disabled","disabled");
			var u = $("#username", form).val();
			var p = $("#password", form).val();
			console.log("click");
			if(u != '' && p!= '') {
				
				//alert(BASE_URL+"LogIn")
				$.ajax({
						url: BASE_URL+"LogIn",
						type: "POST",
						data: JSON.stringify({user_name:u , user_password:p}),  
						
						processData : false,
						contentType: false,
						success: function(data) {
							//alert(data);
							 var json = $.parseJSON(data);
							 
							 //check error existed or no
							 
							 //Here if there is user
							 if(json.error == undefined)
							 {							 
								//alert( json.user.user_name );
								//window.localStorage["username"] = u;
								//window.localStorage["password"] = p;                    
								//$.mobile.changePage("some.html");
								
								
								window.localStorage.setItem('launchCount',1);
								
								localStorage.setItem("user_id", json.user.user_id);
								localStorage.setItem("user_name", json.user.user_id);
								//localStorage.setItem("user_id", json.user.user_id);
								
								/*$("#errspan_user_name").css("color", "green");	
								$("#errspan_password").css("color", "green");*/
								
							
								
								/*$("#username").css("outline", "#428600 solid thin");
								$("#password").css("outline", "#428600 solid thin");*/
								
								 $('#username').val("");
								 $('#password').val("");
								
								
								$.mobile.changePage("#page_categories");
								
								
							 }
							 
							 //Here if theris error in the name or the password
							 else
							 {
								//alert("error:" + json.error); 
								if(json.error == "name")
								{									
									$("#errspan_user_name").css("color", "red");		

									
									$("#username").css("outline", "#BD0000 solid thin");
									
								}
								
								else if (json.error == "password")
								{
									$("#errspan_user_name").css("color", "green");		
									$("#errspan_password").css("color", "red");		
									
									$("#username").css("outline", "#428600 solid thin");
									$("#password").css("outline", "#BD0000 solid thin");
									
								}
								//navigator.notification.alert("Your login failed", function() {});
							 }
							 
							 $("#submitButton").removeAttr("disabled");
							 
						}    
					});
				
				
				
				
			} else {
				//Thanks Igor!
				navigator.notification.alert("You must enter a username and password", function() {});
				$("#submitButton").removeAttr("disabled");
			}
			return false;
		}

		
		
/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------
					End Of Login Handler 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
		
/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
		           Start Of Check Mail In The System Or No
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
		function checkMail() {
			
			var form = $("#checkMailForm"); 
			//disable the button so we can't resubmit while we wait
			$("#checkMailButton",form).attr("disabled","disabled");
			var mail=$("#usermailcheck", form).val();
			
			if(mail != '' ) {
				
				
				$.ajax({
							url: BASE_URL+"CheckMail/"+mail, // point to server-side PHP script 
							//dataType: 'json',  // what to expect back from the PHP script, if anything
							cache: false,
							contentType: false,
							processData: false,

							//data: form_data,                         
							type: 'GET',
							success: function(php_script_response){
							 
							  //alert($.parseJSON(php_script_response));
							 //alert(php_script_response);
							 var json = $.parseJSON(php_script_response);
							
							 //Here if there is user
							 if(json.mail_status == "true")
							 {							 
								alert("check your mail");
								/*window.localStorage["username"] = u;
								window.localStorage["password"] = p;                    
								//$.mobile.changePage("some.html");
								
								
								localStorage.setItem("user_id", json.user.user_id);
								localStorage.setItem("user_name", json.user.user_id);
								//localStorage.setItem("user_id", json.user.user_id);
								$("#errspan_user_name").css("color", "green");	
								$("#errspan_password").css("color", "green");
								
							
								
								$("#username").css("outline", "#428600 solid thin");
								$("#password").css("outline", "#428600 solid thin");
								*/
								$("#errspan_user_mail").css("color", "#428600");		

									
								$("#usermailcheck").css("outline", "#428600 solid thin");
									
								$.mobile.changePage("#page_login");
								
								
							 }
							 
							 //Here if theris error in the name or the password
							 else
							 {
													
									$("#errspan_user_mail").css("color", "red");		

									
									$("#usermailcheck").css("outline", "#BD0000 solid thin");
									
							 }
							 
							 $("#checkMailButton").removeAttr("disabled");
							 
						}
							 });
							 
				
				
				
				
				
				
			} 
			
			else 
			{
				//Thanks Igor!
				navigator.notification.alert("You must enter a username and password", function() {});
				$("#checkMailButton").removeAttr("disabled");
			}
			return false;
		}


/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

   function getStatus() {
      myService.getStatus(function(r){displayResult(r)}, function(e){displayError(e)});
   }

   function displayResult(data) {
      alert("Is service running: " + data.ServiceRunning);
   }

   function displayError(data) {
      alert("We have an error");
   }
   
   
function go() {
   myService.getStatus(function(r){startService(r)}, function(e){handleError(e)});
};

function startService(data) {
	//alert("start service");
   if (data.ServiceRunning) {
      enableTimer(data);
   } else {
      myService.startService(function(r){enableTimer(r)}, function(e){handleError(e)});
   }
}


function enableTimer(data) {
   if (data.TimerEnabled) {
      registerForUpdates(data);
   } else {
      myService.enableTimer(60000, function(r){registerForUpdates(r)}, function(e){handleError(e)});
   }
}

function registerForUpdates(data) {
	//alert("ojk");
   if (!data.RegisteredForUpdates) {
      myService.registerForUpdates(function(r){updateHandler(r)}, function(e){handleError(e)});
   }
}

function handleError(data) {
				alert("Error: " + data.ErrorMessage);
 				alert(JSON.stringify(data));
				//updateView(data);
 			}
			
			
function allDone() {
   alert("Service now running");
}

function updateHandler(data) {
   if (data.LatestResult != null) {
      try {
         var resultMessage = document.getElementById("resultMessage");
         resultMessage.innerHTML = data.LatestResult.Message;
      } catch (err) {
      }
   }
}

   function getStatus() {
      myService.getStatus(function(r){displayResult(r)}, function(e){displayError(e)});
   }

   function displayResult(data) {
      alert("Is service running: " + data.ServiceRunning);
   }

   function displayError(data) {
      alert("We have an error");
   }
   
   
   
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

   /** Set Parameters Section when navigate
   ----------------------------------------------------------------------*/
		//Store Category Id 
	   function setCategoryId(id)
	   {
		   //alert(id);
		   localStorage.setItem("CategoryId",id);
	   }
	   function getCategoryId()
	   {
			localStorage.getItem("CategoryId")
	   }
	   
	   //Store SubCategory Id
	   function setSubCategoryId(id)
	   {
		   //alert(id);
		   localStorage.setItem("SubCategoryId",id);
	   }
	   function getSubCategoryId()
	   {
			localStorage.getItem("SubCategoryId")
	   }
	   
	    //Store Presentation Id WHICH here is the presentation link
	   function setPresentationId(id)
	   {
		   //alert(id);
		   localStorage.setItem("PresentationId",id);
	   }
	   function getPresentationId()
	   {
			localStorage.getItem("PresentationId")
	   }
	   
	   
	   
	  function setPresentationAndOpenLink(dom)
	  {
		  
		
		if ($.mobile.activePage.attr('id') == "page_presentation")
		{
								//alert("page_presentation");
								
								 var user_name = localStorage.getItem("user_name");
								 var user_id = localStorage.getItem("user_id");
								 var presentation_id = dom.getAttribute('id');
								 var currentdate = new Date(); 
								 var current_date_time = currentdate.getDate() + "/"
										+ (currentdate.getMonth()+1)  + "/" 
										+ currentdate.getFullYear() + " @ "  
										+ currentdate.getHours() + ":"  
										+ currentdate.getMinutes() + ":" 
										+ currentdate.getSeconds();
							
								    var lat,lng;
									
									navigator.geolocation.getCurrentPosition(function(position)
															{
																	
																	// just to show how to access latitute and longitude
																	var location = [position.coords.latitude, position.coords.longitude];	
																	lat = position.coords.latitude;
																	lng = position.coords.longitude;
																
															
																
																$.ajax({
																		url: BASE_URL+"Listener",
																		type: "POST",
																		data: JSON.stringify({user_name:user_name , user_id:user_id, presentation_id:presentation_id, date_view: current_date_time, view_lat:lat, view_lng:lng}),  
																		
																		processData : false,
																		contentType: false,
																		success: function(data) {
																			alert(data);							 
																		}    
																	});
											
																},
																function(error)
																{
																	// error getting GPS coordinates
																	//alert('code: ' + error.code + ' with message: ' + error.message + '\n');
																}, 
																{ enableHighAccuracy: true, maximumAge: 3000, timeout: 5000 });
								//alert("user_id:"+user_id);
								//alert("presentation_id:"+presentation_id);
								//alert("date:"+current_date_time);
									
								//alert("it is : " + lat);
								  
											
								  var h =  "http://docs.google.com/viewer?url=" +  encodeURIComponent(dom.getAttribute('data-url')) + "&embedded=true";
								  //alert(h);
								  
								   screen.orientation.lock('landscape');
								   var ref = window.open(h, '_blank', 'location=no, enableviewportscale=no');
								  
								  ref.addEventListener('exit', function(event) 
									{
										
										/*$.mobile.changePage(window.location.href, {
										allowSamePageTransition: true,
										transition: 'none',
										reloadPage: true
									});*/
										screen.orientation.lock('portrait');
									});
								  //$('a.embed').gdocsViewer();
								  //$(id).gdocsViewer();
		}
		
		else if ($.mobile.activePage.attr('id') == "page_sub_categories")
		{
			//alert ("subcategory page");
			
			var user_name = localStorage.getItem("user_name");
								 var user_id = localStorage.getItem("user_id");
								 var presentation_id = dom.getAttribute('id');
								 var currentdate = new Date(); 
								 var current_date_time = currentdate.getDate() + "/"
										+ (currentdate.getMonth()+1)  + "/" 
										+ currentdate.getFullYear() + " @ "  
										+ currentdate.getHours() + ":"  
										+ currentdate.getMinutes() + ":" 
										+ currentdate.getSeconds();
									
								
								var lat,lng;
									
									navigator.geolocation.getCurrentPosition(function(position)
															{
																	
																	// just to show how to access latitute and longitude
																	var location = [position.coords.latitude, position.coords.longitude];	
																	lat = position.coords.latitude;
																	lng = position.coords.longitude;
																
																
																$.ajax({
																		url: BASE_URL+"Listener_no_sub",
																		type: "POST",
																		data: JSON.stringify({user_name:user_name , user_id:user_id, presentation_id:presentation_id, date_view: current_date_time, view_lat:lat, view_lng:lng}),  
																		
																		processData : false,
																		contentType: false,
																		success: function(data) {
																			alert(data);							 
																		}    
																	});
											
																},
																function(error)
																{
																	// error getting GPS coordinates
																	//alert('code: ' + error.code + ' with message: ' + error.message + '\n');
																}, 
																{ enableHighAccuracy: true, maximumAge: 3000, timeout: 5000 });
								//alert("user_id:"+user_id);
								//alert("presentation_id:"+presentation_id);
								//alert("date:"+current_date_time);
									
								//alert("it is : " + lat);
								  
											
								  var h =  "http://docs.google.com/viewer?url=" +  encodeURIComponent(dom.getAttribute('data-url')) + "&embedded=true";
								  //alert(h);
								  
								   screen.orientation.lock('landscape');
								   var ref = window.open(h, '_blank', 'location=no, enableviewportscale=no');
								  
								  ref.addEventListener('exit', function(event) 
									{
										
										/*$.mobile.changePage(window.location.href, {
										allowSamePageTransition: true,
										transition: 'none',
										reloadPage: true
									});*/
										screen.orientation.lock('portrait');
									});
								  //$('a.embed').gdocsViewer();
								  //$(id).gdocsViewer();
		}
			
	  }
		
		
		
		
		
	/** Dynamic Search Button Evenet Handler */
    function buttonUp(){
                var inputVal = $('.searchbox-input').val();
                inputVal = $.trim(inputVal).length;
                if( inputVal !== 0){
                    //$('.searchbox-icon').css('display','none');
                } else {
				
                    $('.searchbox-input').val('');
                    $('.searchbox-icon').css('display','block');
                }
				
            }
			


/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

			
	function checkUpdatesAuto()
	{
				//alert(BASE_URL+"checkUpdatesAuto/android/"+window.localStorage.getItem("update_id"));
				
				$.ajax({
							url: BASE_URL+"checkUpdatesAuto/android/"+window.localStorage.getItem("update_id"), // point to server-side PHP script 
							//dataType: 'json',  // what to expect back from the PHP script, if anything
							cache: false,
							contentType: false,
							processData: false,

							//data: form_data,                         
							type: 'GET',
							success: function(php_script_response){
							 
							 
							 //alert($.parseJSON(php_script_response));
							 //alert(php_script_response);
							 var json = $.parseJSON(php_script_response);
							
							 //Here if there is user
							 if(json.update_status == "true")
							 {							 
								
								/** Change the color of update to red
									     --------------------------------------				**/
								 //alert("true");
								$("#update_icon").css("color", "red");
								//download(json.update_link);
								/*$("#errspan_user_mail").css("color", "#428600");		

									
								$("#usermailcheck").css("outline", "#428600 solid thin");
									
								$.mobile.changePage("#page_login");*/
								
								/*$("#errspan_user_mail").css("color", "#428600");*/
								
							 }
							 
							 //Here if theris error in the name or the password
							 else
							 {
													
									/*$("#errspan_user_mail").css("color", "red");		

									
									$("#usermailcheck").css("outline", "#BD0000 solid thin");*/
									//alert("no updates");
									$("#update_icon").css("color", "grey");
									
							 }
							 
							// $("#checkMailButton").removeAttr("disabled");
							 
						}
							 });
	}
	
	function checkUpdates()
	{
				//alert("check updates");
				$.ajax({
							url: BASE_URL+"checkUpdates/android/"+window.localStorage.getItem("update_id"), // point to server-side PHP script 
							//dataType: 'json',  // what to expect back from the PHP script, if anything
							cache: false,
							contentType: false,
							processData: false,

							//data: form_data,                         
							type: 'GET',
							success: function(php_script_response){
							 
							 
							  //alert($.parseJSON(php_script_response));
							 //alert(php_script_response);
							 var json = $.parseJSON(php_script_response);
							
							 //Here if there is user
							 if(json.update_status == "true")
							 {							 
								//alert("start update");
								/** Code Download And Install Here **/
								//alert(json.update_link);
								downloadAndInstall(json.update_link, json.file_id);
								/*$("#errspan_user_mail").css("color", "#428600");		

									
								$("#usermailcheck").css("outline", "#428600 solid thin");
									
								$.mobile.changePage("#page_login");*/
								
								
							 }
							 
							 //Here if theris error in the name or the password
							 else
							 {
													
									/*$("#errspan_user_mail").css("color", "red");		

									
									$("#usermailcheck").css("outline", "#BD0000 solid thin");*/
									//alert("no updates");
									
							 }
							 
							// $("#checkMailButton").removeAttr("disabled");
							 
						}
							 });
	}
	
	
										/**
											Pages Handling JQM
										--------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/


	$(document).on("pagebeforeshow","#page_login",function(){
			checkUpdatesAuto();	
								
		});
		
	$(document).on("pagebeforeshow","#page_categories",function(){
				angular.element('#category_but').trigger('click');		
								
		});
		

	$(document).on("pagebeforeshow","#page_sub_categories",function(){
				angular.element('#sub_category_but').trigger('click');		
				angular.element('#presentation__no_sub_but').trigger('click');		
					
		});
		
	$(document).on("pagebeforeshow","#page_presentation",function(){
		
				screen.orientation.lock('portrait');
				angular.element('#presentation_but').trigger('click');		
				//alert("presentation before show");
		});

   $(document).on("pagebeforeshow","#page_search_cat",function(){
		
				angular.element('#search_category_but').trigger('click');		
				screen.orientation.lock('portrait');
				
				
		});
		
   $(document).on("pagebeforeshow","#page_search_sub_cat",function(){
		
				angular.element('#search_sub_category_but').trigger('click');		
				screen.orientation.lock('portrait');
				
				
		});
	 $(document).on("pagebeforeshow","#page_search_presentation",function(){
		
				angular.element('#search_presentation_but').trigger('click');		
				screen.orientation.lock('portrait');
				
				
		});
		
	function hexc(colorval) {
			
			if(colorval == "undefined")
			{
				
			}
			
			else
			{
					var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
				delete(parts[0]);
				for (var i = 1; i <= 3; ++i) {
					parts[i] = parseInt(parts[i]).toString(16);
					if (parts[i].length == 1) parts[i] = '0' + parts[i];
				}
				color = '#' + parts.join('');
				return color;
			}
			
			}


	 
	
		
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

		function signout()
		{
			window.localStorage.setItem('launchCount',0);
			window.localStorage.setItem("user_id", "");
			window.localStorage.setItem("user_name", "");
			$.mobile.changePage("#page_login");
		}

		
		
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

	
		/** Download Apk, Ipa and store it at the dircetory 
		--------------------------------------------------------------------------------*/
		function downloadAndInstall(file_link, file_id)
		{	
		
				//alert("s*:" + cordova.file.dataDirectory);
				
				var fileTransfer = new FileTransfer();
				var uri = encodeURI("file:///storage/emulated/0/Download/" + "voda.png");

				$('.waitpage').show();
				//alert("now download: " + file_id);
				fileTransfer.download(	
				//"http://www.phonegaptutorial.com/wp-content/uploads/examples/phonegap-logo.png",
				file_link,
				//cordova.file.externalApplicationStorageDirectory+'whatever.png',
				cordova.file.externalApplicationStorageDirectory+'voda.apk',
				function(theFile){
                        //alert("File Downloaded Successfully " + theFile.toURI());
						promptForUpdateAndroid(theFile, file_id);
						$('.waitpage').hide();
						
                    },function(error){
                        //alert("File Transfer failed" + error.message);
                    });
             
				
		}
			
	

	/** Uses the borismus webintent plugin
	----------------------------------------------------------------------*/
	function promptForUpdateAndroid(entry, file_id) {
		
		//alert(entry.toURL());
		
		//localStorage.setItem();
		window.localStorage.setItem("update_id", file_id);
		$("#update_icon").css("color", "grey");
		
		window.plugins.webintent.startActivity({
				action: window.plugins.webintent.ACTION_VIEW,
				url: entry.toURL() ,
				type: 'application/vnd.android.package-archive'
			},
			function () {
			},
			function () {
				//alert('Failed to open URL via Android Intent.');
				//console.log("Failed to open URL via Android Intent. URL: " + entry.toURL());
			}
		);
	}
		

/** Handling the System Directory Request 
---------------------------------------------------------------------*/		
function fail() {
    //alert("system: failed to get filesystem");
}

function failResovle(error) {
    //alert("resolve: " + error.code);
}

function gotFS(fileSystem) {
    //alert("got filesystem");

        // save the file system for later access
    //alert("gotFS " +fileSystem.root.nativeURL);
    window.rootFS = fileSystem.root;
	
	
	 fileSystem.root.getDirectory("Download", {
                        create: true,
                        exclusive: false
                  }, gotDirectory, fail);
}


  function gotDirectory(entry) {
					
				//alert("Directory is : " + entry.nativeURL);
                 entry.getFile('yourFileNAme', {
                        create: true,
                        exclusive: false
                  }, gotFileEntry, fail);
				  
            }

   function gotFileEntry(fileEntry) {
				//alert("file  is : " + fileEntry.nativeURL);
                  filePath = fileEntry.nativeURL;
				  
				 /* 
                  var fileTransfer = new FileTransfer();
                  fileTransfer.download(
                              "http://www.phonegaptutorial.com/wp-content/uploads/examples/phonegap-logo.png", filePath, function(entry) {
                                    //console.log("success");
									alert("sucesso !")
                              }, function(error) {
                                    //console.log("error");
									alert("Fialo !")
                              }, true, {});
							  */
            }

		
	function onResolveSuccess(fileEntry) {
       // alert("resolve sucess: " + fileEntry.name);
    }

	
/*----------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
													End Of Code 
-------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------*/
	