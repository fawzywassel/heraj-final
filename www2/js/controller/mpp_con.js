	var app = angular.module('Main', ["ngSanitize"]);
	//.module("spike", ["ngAnimate", "ngSanitize"]);
	
	var BASE_URL = "http://192.168.0.104/vodafonebackend/";
	/* 
	Categories Controller
	-------------------------------------------------------------------------------------------------------------------------------------------------*/
	app.controller("LoadCategories", function($scope,$http) {
    $http.get(BASE_URL+"categories/")
	   .success(function(response) 
			  {
				 $scope.names = response;
				$scope.getCategories= function() {
				$('.waitpage').show();
				$http.get(BASE_URL+"categories/")
			   .success(function(response) 
					  {
						  $('.waitpage').hide();
						  $scope.names = response;
						  }
						  );
						}
	
	
	
	
	
	
				  }
			);
    } );
	
	
	/* 
	Sub Categories Controller
	------------------------------------------------------------------------------------------------------------------------------------------------*/
	app.controller("LoadSubCategories", function($scope,$http) {
    $http.get(BASE_URL+"subcategories/"+localStorage.getItem("CategoryId"))
	   .success(function(response) 
			  {
				  
				  //alert(BASE_URL+"subcategories/"+localStorage.getItem("CategoryId"));			
				  $scope.names = response;
			      $scope.getSubCategories= function() {
				  $('.waitpage').show();
				  $http.get(BASE_URL+"subcategories/"+localStorage.getItem("CategoryId"))
			      .success(function(response) 
					  {
						  $('.waitpage').hide();
						 $scope.names = response;
						  }
						  );
						}
	
				  }
			);
    } );
	
	
	
	/* 
	Presentations Controller
	----------------------------------------------------------------------------------------------------------------------------------------------------*/
	app.controller("LoadPresentations", function($scope,$http) {
    $http.get(BASE_URL+"presentation/"+localStorage.getItem("SubCategoryId"))
	   .success(function(response) 
			  {
				  
				  //alert(BASE_URL+"presentation/"+localStorage.getItem("SubCategoryId"));			
				  $scope.names = response;
			      $scope.getPresentations= function() {
					  $('.waitpage').show();
				  $http.get(BASE_URL+"presentation/"+localStorage.getItem("SubCategoryId"))
			      .success(function(response) 
					  {
						 //alert(BASE_URL+"presentation/"+localStorage.getItem("SubCategoryId"));
						 $('.waitpage').hide();
						 $scope.names = response;
						  }
						  );
						}
	
				  }
			);
    } );
	
	
	
	
	/* 
	Search Controller For categories search
	-----------------------------------------------------------------------------------------------------------------------------------------------------*/
	app.controller("LoadSearchResultsCats", function($scope,$http) {
	//alert(BASE_URL+"searchInCat/"+localStorage.getItem("search_keyword"));
    $http.get(BASE_URL+"searchInCat/"+localStorage.getItem("search_keyword"))
	   .success(function(response) 
			  {
				
				$scope.names = response;
				$scope.getSearchCategories= function() {
					$('.waitpage').show();
					$http.get(BASE_URL+"searchInCat/"+localStorage.getItem("search_keyword"))
					.success(function(response) 
						  {
							    //alert("search_function");
								$('.waitpage').hide();
								$scope.names = response;
							  }
							  );
							}
					}
			);
    } );
	
	
	/* 
	Search Controller For sub categories search
	----------------------------------------------------------------------------------------------------------------------------------------------*/
	app.controller("LoadSearchResultsSubCats", function($scope,$http) {
	//alert(BASE_URL+"searchInCat/"+localStorage.getItem("search_keyword"));
    $http.get(BASE_URL+"searchInSubCat/"+localStorage.getItem("search_keyword"))
	   .success(function(response) 
			  {
				
				$scope.names = response;
				$scope.getSearchSubCategories= function() {
					$('.waitpage').show();
				  $http.get(BASE_URL+"searchInSubCat/"+localStorage.getItem("search_keyword"))
			      .success(function(response) 
					  {
						  //alert(BASE_URL+"searchInSubCat/"+localStorage.getItem("search_keyword"));
						  $('.waitpage').hide();
						 $scope.names = response;
						  }
						  );
						}
						
			
					}
			);
    } );
	
	
		/* 
	Search Controller For presentation search
	----------------------------------------------------------------------------------------------------------------------------------------------------*/
	app.controller("LoadSearchResultsPresentation", function($scope,$http) {
	//alert(BASE_URL+"searchInCat/"+localStorage.getItem("search_keyword"));
    $http.get(BASE_URL+"searchInPresentation/"+localStorage.getItem("search_keyword"))
	   .success(function(response) 
			  {
				
				$scope.names = response;
				$scope.getSearchPresentation= function() {
					$('.waitpage').show();
				  $http.get(BASE_URL+"searchInPresentation/"+localStorage.getItem("search_keyword"))
			      .success(function(response) 
					  {
						  $('.waitpage').hide();
						 $scope.names = response;
						  }
						  );
						}
						
			
					}
			);
    } );
	

		/* 
	Presentations  with no subcategories Controller
	-------------------------------------------------------------------------------------------------------------------------------*/
	app.controller("LoadPresentationsInCat", function($scope,$http) {
    $http.get(BASE_URL+"presentationNoSub/"+localStorage.getItem("CategoryId"))
	   .success(function(response) 
			  {
				 $scope.names = response;
				$scope.getPresentations= function() {
					$('.waitpage').show();
				$http.get(BASE_URL+"presentationNoSub/"+localStorage.getItem("CategoryId"))
			   .success(function(response) 
					  {
						  $('.waitpage').hide();
						  $scope.names = response;
						  }
						  );
						}
	
				  }
			);
    } );
	
