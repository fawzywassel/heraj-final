		/** Drawer Manipulation 
		****************************************************************************************************
		*/
		
		$(document).on("pagebeforeshow","#about",function(){ // When entering pagetwo
		
					$("#myPanel8").on("panelbeforeopen", function (event, ui) { 
						$('.drawer_width').css("display", "block");
					});

					$("#myPanel8").on("panelbeforeclose", function (event, ui) {
						$('#drawer_width').css("display", "none");
					});
	    
		});
		
		$(document).on("pagebeforeshow","#about_ar",function(){ // When entering pagetwo
		
					$("#myPanel8_ar").on("panelbeforeopen", function (event, ui) { 
						$('.drawer_width').css("display", "block");
					});

					$("#myPanel8_ar").on("panelbeforeclose", function (event, ui) {
						$('#drawer_width').css("display", "none");
					});
	    
		});
		
		$(document).on("pagebeforeshow","#talk_us",function(){ // When entering pagetwo
		
					$("#myPanel9").on("panelbeforeopen", function (event, ui) { 
						$('.drawer_width').css("display", "block");
					});

					$("#myPanel9").on("panelbeforeclose", function (event, ui) {
						$('#drawer_width').css("display", "none");
					});
	    
		});
		
		$(document).on("pagebeforeshow","#talk_us_ar",function(){ // When entering pagetwo
		
					$("#myPanel9_ar").on("panelbeforeopen", function (event, ui) { 
						$('.drawer_width').css("display", "block");
					});

					$("#myPanel9_ar").on("panelbeforeclose", function (event, ui) {
						$('#drawer_width').css("display", "none");
					});
	    
		});
		
		
		$(document).on("pagebeforeshow","#mpp_loc",function(){ // When entering pagetwo
		
					$("#myPanel10").on("panelbeforeopen", function (event, ui) { 
						$('.drawer_width').css("display", "block");
					});

					$("#myPanel10").on("panelbeforeclose", function (event, ui) {
						$('#drawer_width').css("display", "none");
					});
	    
		});
		
		$(document).on("pagebeforeshow","#mpp_loc_ar",function(){ // When entering pagetwo
		
					$("#myPanel10_ar").on("panelbeforeopen", function (event, ui) { 
						$('.drawer_width').css("display", "block");
					});

					$("#myPanel10_ar").on("panelbeforeclose", function (event, ui) {
						$('#drawer_width').css("display", "none");
					});
	    
		});
		
		
		$(document).on("pagebeforeshow","#member",function(){ // When entering pagetwo
		
					$("#myPanel11").on("panelbeforeopen", function (event, ui) { 
						$('.drawer_width').css("display", "block");
					});

					$("#myPanel11").on("panelbeforeclose", function (event, ui) {
						$('#drawer_width').css("display", "none");
					});
	    
		});
		
		$(document).on("pagebeforeshow","#member_ar",function(){ // When entering pagetwo
		
					$("#myPanel11_ar").on("panelbeforeopen", function (event, ui) { 
						$('.drawer_width').css("display", "block");
					});

					$("#myPanel11_ar").on("panelbeforeclose", function (event, ui) {
						$('#drawer_width').css("display", "none");
					});
	    
		});
		
		
		
		$(document).on("pagebeforeshow","#geo_offers",function(){ // When entering pagetwo
		
					$("#myPanel12").on("panelbeforeopen", function (event, ui) { 
						$('.drawer_width').css("display", "block");
					});

					$("#myPanel12").on("panelbeforeclose", function (event, ui) {
						$('#drawer_width').css("display", "none");
					});
	    
		});
		
		$(document).on("pagebeforeshow","#geo_offers_ar",function(){ // When entering pagetwo
		
					$("#myPanel12_ar").on("panelbeforeopen", function (event, ui) { 
						$('.drawer_width').css("display", "block");
					});

					$("#myPanel12_ar").on("panelbeforeclose", function (event, ui) {
						$('#drawer_width').css("display", "none");
					});
	    
		});
		
		$(document).on("pagebeforeshow","#search_results",function(){ // When entering pagetwo
		
					$("#myPanel14").on("panelbeforeopen", function (event, ui) { 
						$('.drawer_width').css("display", "block");
					});

					$("#myPanel14").on("panelbeforeclose", function (event, ui) {
						$('#drawer_width').css("display", "none");
					});
	    
		});
		
		
		$(document).on("pagebeforeshow","#search_results_ar",function(){ // When entering pagetwo
		
					$("#myPanel14_ar").on("panelbeforeopen", function (event, ui) { 
						$('.drawer_width').css("display", "block");
					});

					$("#myPanel14_ar").on("panelbeforeclose", function (event, ui) {
						$('#drawer_width').css("display", "none");
					});
	    
		});
		
		
		$(document).on("pagebeforeshow","#news_results",function(){ // When entering pagetwo
		
					$("#myPanel18").on("panelbeforeopen", function (event, ui) { 
						$('.drawer_width').css("display", "block");
					});

					$("#myPanel18").on("panelbeforeclose", function (event, ui) {
						$('#drawer_width').css("display", "none");
					});
	    
		});
		
		$(document).on("pagebeforeshow","#news_results_ar",function(){ // When entering pagetwo
		
					$("#myPanel18_ar").on("panelbeforeopen", function (event, ui) { 
						$('.drawer_width').css("display", "block");
					});

					$("#myPanel18_ar").on("panelbeforeclose", function (event, ui) {
						$('#drawer_width').css("display", "none");
					});
	    
		});
		
		$(document).on("pagebeforeshow","#careers",function(){ // When entering pagetwo
		
					$("#myPanel21").on("panelbeforeopen", function (event, ui) { 
						$('.drawer_width').css("display", "block");
					});

					$("#myPanel21").on("panelbeforeclose", function (event, ui) {
						$('#drawer_width').css("display", "none");
					});
	    
		});
		
		$(document).on("pagebeforeshow","#careers_ar",function(){ // When entering pagetwo
		
					$("#myPanel21_ar").on("panelbeforeopen", function (event, ui) { 
						$('.drawer_width').css("display", "block");
					});

					$("#myPanel21_ar").on("panelbeforeclose", function (event, ui) {
						$('#drawer_width').css("display", "none");
					});
	    
		});
		
		$(document).on("pagebeforeshow","#fav_results",function(){ // When entering pagetwo
		
					$("#myPanel22").on("panelbeforeopen", function (event, ui) { 
						$('.drawer_width').css("display", "block");
					});

					$("#myPanel22").on("panelbeforeclose", function (event, ui) {
						$('#drawer_width').css("display", "none");
					});
	    
		});
		
		$(document).on("pagebeforeshow","#fav_results_ar",function(){ // When entering pagetwo
		
					$("#myPanel22_ar").on("panelbeforeopen", function (event, ui) { 
						$('.drawer_width').css("display", "block");
					});

					$("#myPanel22_ar").on("panelbeforeclose", function (event, ui) {
						$('#drawer_width').css("display", "none");
					});
	    
		});